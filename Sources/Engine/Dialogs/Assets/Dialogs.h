#ifndef DEF_DIALOGS

#define DEF_DIALOGS

#include "../DialogManager.h"
#include "../../Tags_Layers/Tags.h"

/** Dialog Magic Sword **/

// Dialog sentence magic sword 0
extern const DialogSentence DialogSentence_MagicSword0;

// Dialog sentence magic sword 1
extern const DialogSentence DialogSentence_MagicSword1;

#endif