#ifndef DEF_DIALOGMANAGER

#define DEF_DIALOGMANAGER

#include <gb/gb.h>

#define DIALOG_TILESET_NBTILE 50
#define DIALOG_WINDOW_X 7
#define DIALOG_WINDOW_Y 104
#define DIALOG_WINDOW_WIDTH 20
#define DIALOG_WINDOW_HEIGHT 5
#define DIALOG_WINDOW_NBTILE 100

/** Structs **/

// Struct used to define a dialog sentence
struct DialogSentence
{
    // Tiles to draw
    unsigned char tiles[DIALOG_WINDOW_NBTILE];

    // Next dialog sentence
    struct DialogSentence* nextDialogSentence;
};
typedef struct DialogSentence DialogSentence;

// Struct defining a dialog
struct Dialog
{
    // Tag used to identify this dialog (should be set from Tags.h)
    UINT8 tag;

    // Pointer on active sentence
    DialogSentence* activeSentence;
};
typedef struct Dialog Dialog;

#include "Assets/Dialogs.h"

/** Exposed Functions **/

/*
* Set sepcified dialog as active dialog and open dialog window. Bind callback to dialog closed too
*/
void DialogManager_Open(Dialog* _Dialog, void (*_Callback)(Dialog*));

/*
* Draw next dialog sentence in window
*/
void DialogManager_Next();

/*
* Reset active dialog pointer to NULL and close dialog window
*/
void DialogManager_Close();

/*
* Check if an active dialog exists
*/
UINT8 DialogManager_DialogExists();

/*
* Bind a callback to dialog closed pointer
*/
void DialogManager_BindCallbackToDialogClosed(void (*_Callback)(Dialog*));

/** Static Functions **/

/*
* Draw a sentence to dialog window
*/
void DialogManager_DrawSentence();

/*
* Copy dialog to data from a dialog to another
*/
void DialogManager_CopyDialog(Dialog* _Source, Dialog* _Destination);

/** Static Attributes **/

// Active dialog
static Dialog s_ActiveDialog;

// Pointer on function called when closing a dialog
static void (*s_onDialogClosed)(Dialog*);

/** Const Attributes **/

// Tileset to use for dialog
extern const unsigned char DialogTileset[];

#endif