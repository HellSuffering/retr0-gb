#include "Game.h"

#include "../Movables/Movable.h"
#include "../Inputs/InputManager.h"
#include "../Maps/MapLoader.h"
#include "../Maps/MapsCallbacks.h"
#include "../Scroll/ScrollManager.h"
#include "../Audio/MusicPlayer.h"

/*
* Perform initialization
*/
void Game_Initialize()
{
    // Turn on the display
	DISPLAY_ON;

    // Hide window
    HIDE_WIN;

    // Show sprites
	SHOW_SPRITES;

    // Show background
	SHOW_BKG;

    // Initialize movable module
    Movable_Initialization();

    // Initialize all sprite animations
    SpritesAnimations_Initialization();

    // Load map
    MapLoader_RequestLoadMapData(MAP16x16_LOAD_PARAMS, NULL);

    // Use VBLANK interrupt to update game loop
    disable_interrupts();
    add_VBL(Game_UpdateGameLoop);
    enable_interrupts();
    set_interrupts(VBL_IFLAG);
}

/*
* Game loop
*/
void Game_UpdateGameLoop()
{
    // Pre update callback
    if (s_PreUpdate != NULL)
        (*s_PreUpdate)();

    // Update scroll
    ScrollManager_ScrollToDesiredOffset();

    // Input manager update
    InputManager_Update();

    // Movable update
    Movable_Update();

    // Music player update
    MusicPlayer_Update();

    // Map update callback
    if (s_MapUpdate != NULL)
        (*s_MapUpdate)();

    // Post update callback
    if (s_PostUpdate != NULL)
        (*s_PostUpdate)();			

    // Check for map loading
    MapLoader_LoadPendingMap();
}

/*
* Bind callback to pre update function pointer
*/
void Game_BindPreUpdateCallback(void (*_Callback)(void))
{
    s_PreUpdate = _Callback;
}

/*
* Bind callback to post update function pointer
*/
void Game_BindPostUpdateCallback(void (*_Callback)(void))
{
    s_PostUpdate = _Callback;
}

/*
* Bind callback to map update function pointer
*/
void Game_BindMapUpdateCallback(void (*_Callback)(void))
{
    s_MapUpdate = _Callback;
}