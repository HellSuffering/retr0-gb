#ifndef DEF_GAME

#define DEF_GAME

#define GAME_DELAY 20

#include <gb/gb.h>

/** Exposed Functions **/

/*
* Perform initialization
*/
void Game_Initialize();

/*
* Game loop
*/
void Game_UpdateGameLoop();

/*
* Bind callback to pre update function pointer
*/
void Game_BindPreUpdateCallback(void (*_Callback)(void));

/*
* Bind callback to post update function pointer
*/
void Game_BindPostUpdateCallback(void (*_Callback)(void));

/*
* Bind callback to map update function pointer
*/
void Game_BindMapUpdateCallback(void (*_Callback)(void));

/** Static Variables **/

// Pointer on function called on game loop begin before game functions
static void (*s_PreUpdate)(void) = NULL;

// Pointer on function called on game loop after game functions
static void (*s_PostUpdate)(void) = NULL;

// Pointer on function called on game loop to update current map (use this to run custom code for each map)
static void (*s_MapUpdate)(void) = NULL;

#endif