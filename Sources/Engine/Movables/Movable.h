#ifndef DEF_MOVABLE

#define DEF_MOVABLE

#define MOVABLE_LIMIT 10

#include <gb/gb.h>

#include "../Math/Vector.h"
#include "../Math/Box.h"
#include "../Sprites/Sprite.h"

// Enum used to define how movable react to potential collision when overlapping an other movable
enum ECollisionMode
{
    COLLISIONMODE_NOCOLLISION,
    COLLISIONMODE_COLLISION,
    COLLISIONMODE_OVERLAP
};
typedef enum ECollisionMode ECollisionMode;

// Enum used to define relationship with scrolling
enum EScrollRule
{
    SCROLLRULE_NOSCROLL,
    SCROLLRULE_CANSCROLL,
    SCROLLRULE_SCROLLREFERENCE
};
typedef enum EScrollRule EScrollRule;

// Struct used to define all data for a movable element
struct Movable
{
    // Sprite used to draw the movable on screen
    Sprite leftSprite;

    // Second sprite used to draw the movable on screen (allow to have a 16 width sprite)
    Sprite rightSprite;

    // Box used to define movable position, with and height on map
    Box box;

    // Collision mode use for collision detection for this movable
    ECollisionMode collisionMode;

    // Number used to identify the "group" belongs the movable (it is recomended to use custom #define in Tags.h)
    unsigned int tag;

    // Number used to define the collision layer of the movable (its is recommended to use #define set in Layers.h)
    UINT8 layer;

    // Bitmask used to define which layer is allowed to collide with the movable
    unsigned int layerMask;

    // Relationship with scrolling
    EScrollRule scrollRule;

    // Is the movable the reference for scrolling

    // Is movable enabled
    UINT8 enabled;

    // Constrain position to snap with map tiles
    UINT8 constrainOnTiles;

    // Pointer on function used for callback on overlap with an other movable
    void (*onOverlap)(struct Movable* _Self, struct Movable* _Other);
};
typedef struct Movable Movable;

#include "../Collision/Collision.h"


/** Exposed Functions **/

/*
* Perform global initialization as initiliazing movable array
*/
void Movable_Initialization();

/*
* Called every frame. Perfom update operations as sprite update for constrained to tiles movables
*/
void Movable_Update();

/*
* Set movable position. Check collision before moving
*/
void Movable_SetPosition(Movable* _Movable, Vector* _Position, UINT8 _CheckCollision, UINT8 _FromScroll);

/*
* Add translation vector to current movable position. Check collision before moving
*/
void Movable_Translate(Movable* _Movable, Vector* _TranslationVector, UINT8 _CheckCollision);

/*
* Perform a translation but ignore tile constraint (scoll is leading the movement)
*/
void Movable_Scroll(Movable* _Movable, Vector* _TranslationVector);

/*
* Get movable position getting box top left corner position
*/
void Movable_GetPosition(Movable* _Movable, Vector* _OutPosition);

/*
* Initialize a the first disabled movable found with specified value and return on pointer on it. 
* Be careful, once your movable is disabled, pointer is not valid anymore (can point on a disabled movable or on an other movable)
*/
Movable* Movable_CreateMovable(UINT8 _TileIDLeft, UINT8 _TileIDRight, Box* _Box, ECollisionMode _CollisionMode, UINT8 _Tag, UINT8 _Layer, unsigned int _LayerMask, EScrollRule _ScrollRule, UINT8 _ConstrainOnTiles);

/*
* Disable a movable
*/
void Movable_Destroy(Movable* _Movable);

/*
* Disable all movables
*/
void Movable_DestroyAll();

/*
* Get array of all movables (you should check which movable is enabled and which is disabled, you should not need to work with disabled movables)
*/
Movable* Movable_GetMovables();

/*
* Find movable of specified tag and return it
*/
Movable* Movable_FindMovable(UINT8 _Tag);

/*
* Set sprite anim on movable sprites
*/
void Movable_SetSpriteAnim(Movable* _Movable, SpriteAnim* _LeftSpriteAnim, SpriteAnim* _RightSpriteAnim);

/*
* Check if movbale can try moving checking if sprite is at desired position and scroll is at desired offset
*/
UINT8 Movable_CanTryMoving(Movable* _Movable);

/** Static Functions **/

/*
* Set all values for movable struct and perform initialisation as adding movable pointer to array
*/
static void Movable_InitializeMovable(Movable* _Movable, UINT8 _TileIDLeft, UINT8 _TileIDRight, Box* _Box, ECollisionMode _CollisionMode, UINT8 _Tag, UINT8 _Layer, unsigned int _LayerMask, EScrollRule _ScrollRule, UINT8 _ConstrainOnTiles);

/** Global Variables **/

// Array of all movables
extern Movable s_MovableArray[MOVABLE_LIMIT];

#endif