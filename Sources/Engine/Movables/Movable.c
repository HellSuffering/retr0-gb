#include "Movable.h"

#include "../Scroll/ScrollManager.h"
#include "../Maps/MapLoader.h"
#include "../Helpers/SpaceHelper.h"

// Array of all movables
Movable s_MovableArray[MOVABLE_LIMIT];

/*
* Perform global initialization as initiliazing movable array
*/
void Movable_Initialization()
{
    // Local variables
    UINT8 i;
    Movable* movable = s_MovableArray;
    
    // Initialize movable array
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        movable->leftSprite.ID = 0;
        movable->rightSprite.ID = 0;
        movable->enabled = FALSE;

        movable++;
    }
}

/*
* Called every frame. Perfom update operations as sprite update for constrained to tiles movables
*/
void Movable_Update()
{
    // Local variables
    UINT8 i;
    Movable* movable = s_MovableArray;
    
    // Iterate on all movables
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        if (movable->enabled)
        {
            // Update sprite
            Sprite_UpdateSprite(&movable->leftSprite);
            Sprite_UpdateSprite(&movable->rightSprite);
        }

        movable++;
    }
}

/*
* Set movable position. Check collision before moving
*/
void Movable_SetPosition(Movable* _Movable, Vector* _Position, UINT8 _CheckCollision, UINT8 _FromScroll)
{
    // Local variables
    Vector oldPosition;
    Vector checkMapCollisionPosition;
    CollisionResult colResult;
    UINT8 canMove = TRUE;
    UINT8 canScroll = FALSE;
    Vector translation;

    if (_Movable != NULL && _Position != NULL)
    {
        // Keep position before moving
        Box_GetPosition(&_Movable->box, &oldPosition);

        // Get translation
        Vector_Subtract(_Position, &oldPosition, &translation);

        // Adjust position to snap on tiles
        if (_Movable->constrainOnTiles && !_FromScroll)
        {
            SpaceHelper_ScreenSpaceToTileSpace(_Position, _Position);
            SpaceHelper_TileSpaceToScreenSpace(_Position, _Position);
        }

        // Check if movable is the scrolling reference
        if (_Movable->scrollRule == SCROLLRULE_SCROLLREFERENCE && !_FromScroll)
            canScroll = ScrollManager_CanScroll(_Position, &translation);

        // Check collision
        if (_CheckCollision)
        {
            Vector_CopyTo(_Position, &checkMapCollisionPosition);
                    
            // Get next position on map for map collision
            if (_Movable->box.width != 8 || _Movable->box.height != 8)
            {
                if (translation.x > 0)
                    checkMapCollisionPosition.x = _Movable->box.x + _Movable->box.width;
                
                if (translation.y > 0)
                    checkMapCollisionPosition.y = _Movable->box.y + _Movable->box.height;  
            } 
            
            if (Collision_CheckCollisionForMovable(_Movable, _Position, &checkMapCollisionPosition, &colResult))
            {
                if (colResult.isBlocking)
                    canMove = FALSE;
            }
        }

        // Move
        if (canMove)
        {
            if (canScroll)
            {
                ScrollManager_AddDesiredScrollOffset(&translation);
            }
            else
            {
                Box_SetPosition(&_Movable->box, _Position);
                Sprite_SetDesiredPosition(&_Movable->leftSprite, _Position);
                Sprite_SetDesiredPosition(&_Movable->rightSprite, _Position);
            }
        }
    }
}

/*
* Add translation vector to current movable position. Check collision before moving
*/
void Movable_Translate(Movable* _Movable, Vector* _TranslationVector, UINT8 _CheckCollision)
{
    // Local variables
    Vector newPosition;
    Vector movablePosition;

    if ((!_CheckCollision || Movable_CanTryMoving(_Movable)) && _TranslationVector != NULL)
    {
        if (_TranslationVector->x != 0 || _TranslationVector->y != 0)
        {
            if (_Movable->constrainOnTiles)
            {
                _TranslationVector->x  =_TranslationVector->x << 3;
                _TranslationVector->y  =_TranslationVector->y << 3;
            }

            // Get new movable position
            Movable_GetPosition(_Movable, &movablePosition);
            Vector_Add(_TranslationVector, &movablePosition, &newPosition);

            // Move movable to new position
            Movable_SetPosition(_Movable, &newPosition, _CheckCollision, FALSE);
        }
    }
}

/*
* Perform a translation but ignore tile constraint (scoll is leading the movement)
*/
void Movable_Scroll(Movable* _Movable, Vector* _TranslationVector)
{
    // Local variables
    Vector newPosition;
    Vector movablePosition;

    if (_TranslationVector != NULL)
    {
        if (_TranslationVector->x != 0 || _TranslationVector->y != 0)
        {
            // Get new movable position
            Movable_GetPosition(_Movable, &movablePosition);
            Vector_Add(_TranslationVector, &movablePosition, &newPosition);

            // Move movable to new position
            Box_SetPosition(&_Movable->box, &newPosition);
            Sprite_SetDesiredPosition(&_Movable->leftSprite, &newPosition);
            Sprite_SetDesiredPosition(&_Movable->rightSprite, &newPosition);
        }
    }
}

/*
* Get movable position getting box top left corner position
*/
void Movable_GetPosition(Movable* _Movable, Vector* _OutPosition)
{
    if (_OutPosition != NULL)
    {
        if (_Movable != NULL)
            Box_GetPosition(&_Movable->box, _OutPosition);
        else
            Vector_GetVectorZero(_OutPosition);
    }
}

/*
* Initialize a the first disabled movable found with specified value and return on pointer on it. 
* Be careful, once your movable is disabled, pointer is not valid anymore (can point on a disabled movable or on an other movable)
*/
Movable* Movable_CreateMovable(UINT8 _TileIDLeft, UINT8 _TileIDRight, Box* _Box, ECollisionMode _CollisionMode, UINT8 _Tag, UINT8 _Layer, unsigned int _LayerMask, EScrollRule _ScrollRule, UINT8 _ConstrainOnTiles)
{
    // Local variables
    UINT8 i;
    Movable* movable = NULL;

    // Get first disabled movable and initialize it
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        movable = &s_MovableArray[i];

        if (movable != NULL && !movable->enabled)
        {
            Movable_InitializeMovable(movable, _TileIDLeft, _TileIDRight, _Box, _CollisionMode, _Tag, _Layer, _LayerMask, _ScrollRule, _ConstrainOnTiles);

            return movable;
        }
    }
}

/*
* Disable a movable
*/
void Movable_Destroy(Movable* _Movable)
{
    // Local variables
    Vector outScreenPosition = {240,240};

    if (_Movable != NULL && _Movable->enabled)
    {
        // Reset values
        _Movable->enabled = FALSE;
        _Movable->onOverlap = NULL;
        _Movable->collisionMode = COLLISIONMODE_NOCOLLISION;
        _Movable->tag = TAG_DEFAULT;

        // Set movable out screen
        Movable_SetPosition(_Movable, &outScreenPosition, FALSE, FALSE);

        // Free sprites
        if (_Movable->leftSprite.ID != SPRITES_NOTILE)
        {
            move_sprite(_Movable->leftSprite.ID, outScreenPosition.x, outScreenPosition.y);
            Sprite_FreeID(_Movable->leftSprite.ID);
        }

        if (_Movable->rightSprite.ID != SPRITES_NOTILE)
        {
            move_sprite(_Movable->rightSprite.ID, outScreenPosition.x, outScreenPosition.y);
            Sprite_FreeID(_Movable->rightSprite.ID);
        }
    }
}

/*
* Disable all movables
*/
void Movable_DestroyAll()
{
    // Local variables
    UINT8 i;
    Movable* movable = s_MovableArray;

    // Destroy all movoables
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        Movable_Destroy(movable);

        movable++;
    }
}

/*
* Get array of all movables (you should check which movable is enabled and which is disabled, you should not need to work with disabled movables)
*/
Movable* Movable_GetMovables()
{
    return s_MovableArray;
}

/*
* Find movable of specified tag and return it
*/
Movable* Movable_FindMovable(UINT8 _Tag)
{
    // Local variables
    UINT8 i;
    Movable* movable = s_MovableArray;

    // Search movable of specified tag
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        if (movable->tag == _Tag)
        {
            return movable;
        }
        else
        {
            movable++;
        }
    }

    return NULL;
}

/*
* Check if movbale can try moving checking if sprite is at desired position and scroll is at desired offset
*/
UINT8 Movable_CanTryMoving(Movable* _Movable)
{
    if (_Movable != NULL && _Movable->enabled)
    {
        // Check sprite is at desired position
        if (Sprite_IsAtDesiredPosition(&_Movable->leftSprite))
        {
            // Check scroll is at desired offset
            if (ScrollManager_IsAtDesiredOffset())
                return TRUE;
        }
    }

    return FALSE;
}

/*
* Set all values for movable struct and perform initialisation as adding movable pointer to array
*/
static void Movable_InitializeMovable(Movable* _Movable, UINT8 _TileIDLeft, UINT8 _TileIDRight, Box* _Box, ECollisionMode _CollisionMode, UINT8 _Tag, UINT8 _Layer, unsigned int _LayerMask, EScrollRule _ScrollRule, UINT8 _ConstrainOnTiles)
{
    // Local variables
    Vector position, scrollOffset;
    UINT8 mapWidth = MapLoader_GetLoadedMapData()->mapWidth;
    UINT8 mapHeight = MapLoader_GetLoadedMapData()->mapHeight;

    if (_Movable != NULL && _Box != NULL)
    {
        // Initialize values with passed parameters
        _Movable->enabled = TRUE;
        Box_Copy(_Box, &_Movable->box);
        _Movable->collisionMode = _CollisionMode;
        _Movable->tag = _Tag;
        _Movable->layer = _Layer;
        _Movable->layerMask = _LayerMask;
        _Movable->scrollRule = SCROLLRULE_NOSCROLL;
        _Movable->constrainOnTiles = _ConstrainOnTiles;

        // Initialize sprites
        Sprite_SetSpriteData(&_Movable->leftSprite, Sprite_GetFirstFreeSpriteID(), _TileIDLeft, FALSE);
        _Movable->leftSprite.spriteAnim = NULL;

        Sprite_SetSpriteData(&_Movable->rightSprite, Sprite_GetFirstFreeSpriteID(), _TileIDRight, TRUE);
        _Movable->rightSprite.spriteAnim = NULL;

        // Update sprite position
        Box_GetPosition(_Box, &position);
        Vector_Subtract(&position, ScrollManager_GetScrollOffset(), &position);
        Movable_SetPosition(_Movable, &position, FALSE, FALSE);
        Sprite_MoveSprite(&_Movable->leftSprite, &position);
        Sprite_MoveSprite(&_Movable->rightSprite, &position);

        // Set scroll rule after movable has been moved to position, avoid SetPosition function to take care of scroll rule (in case of scroll reference)
        _Movable->scrollRule = _ScrollRule;

        // Initialize default scroll offset to center the scroll reference movable at creation if needed
        if (_ScrollRule == SCROLLRULE_SCROLLREFERENCE)
        {
            // Ensure map is large enough to allow scroll
            if (mapWidth > 20 && mapHeight > 18)
            {
                // Get scroll offset to use to center view on movable
                SpaceHelper_ScreenSpaceToTileSpace(&position, &position);

                if (position.x >= 10)
                {
                    if ((mapWidth - position.x) > 10)
                        scrollOffset.x = position.x - 10;
                    else
                        scrollOffset.x = mapWidth - 20;
                }
                else
                {
                    scrollOffset.x = 0;
                }

                if (position.y >= 9)
                {
                    if ((mapHeight - position.y) > 9)
                        scrollOffset.y = position.y - 10;
                    else
                        scrollOffset.y = mapHeight - 18;
                }
                else
                {
                    scrollOffset.y = 0;
                }

                // Center view on movable
                SpaceHelper_TileSpaceToScreenSpace(&scrollOffset, &scrollOffset);
                SpaceHelper_TileSpaceToScreenSpace(&position, &position);

                if ((scrollOffset.x != 0 || scrollOffset.y != 0))
                {
                    _Movable->scrollRule = SCROLLRULE_CANSCROLL;

                    ScrollManager_AddDesiredScrollOffset(&scrollOffset);
                    ScrollManager_Scroll(&scrollOffset);

                    _Movable->scrollRule = SCROLLRULE_SCROLLREFERENCE;
                    Box_GetPosition(&_Movable->box, &position);
                    Movable_SetPosition(_Movable, &position, FALSE, TRUE);
                    Sprite_MoveSprite(&_Movable->leftSprite, &position);
                    Sprite_MoveSprite(&_Movable->rightSprite, &position);
                }
            }
        }
    }
}

/*
* Set sprite anim on movable sprites
*/
void Movable_SetSpriteAnim(Movable* _Movable, SpriteAnim* _LeftSpriteAnim, SpriteAnim* _RightSpriteAnim)
{
    if (_Movable != NULL)
    {
        _Movable->leftSprite.spriteAnim = _LeftSpriteAnim;
        _Movable->rightSprite.spriteAnim = _RightSpriteAnim;
    }
}