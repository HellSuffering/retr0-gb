#ifndef DEF_SONGS

#define DEF_SONGS

// Patterns
extern const unsigned char Song_Pattern0[];
extern const unsigned char Song_Pattern1[];

// Songs
extern const unsigned char* Song0[];

#endif