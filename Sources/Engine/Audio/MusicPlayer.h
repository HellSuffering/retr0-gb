#ifndef DEF_MUSICPLAYER

#define DEF_MUSICPLAYER

#include <gb/gb.h>
#include "Assets/Songs.h"

// Play specified music at specified speed. If loop == 1, play music in loop
void MusicPlayer_Play(unsigned char* _MusicData, UINT8 _Speed, UINT8 _Loop);

// Pause  music playing
void MusicPlayer_Pause();

// Resume music playing
void MusicPlayer_Resume();

// Stop music and turns off sound system
void MusicPlayer_Stop();

// Update gbt player. Make music continue playing
void MusicPlayer_Update();

#endif