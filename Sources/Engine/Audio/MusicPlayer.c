#include "MusicPlayer.h"

#include "../../ThirdParty/gbtplayer/gbt_player.h"

// Play specified music at specified speed. If loop == 1, play music in loop
void MusicPlayer_Play(unsigned char* _MusicData, UINT8 _Speed, UINT8 _Loop)
{
    gbt_play(_MusicData, 0, _Speed);
    gbt_loop(_Loop);
}

// Pause  music playing
void MusicPlayer_Pause()
{
    gbt_pause(TRUE);
}

// Resume music playing
void MusicPlayer_Resume()
{
    gbt_pause(FALSE);
}

// Stop music and turns off sound system
void MusicPlayer_Stop()
{
    gbt_stop();
}

// Update gbt player. Make music continue playing
void MusicPlayer_Update()
{
    gbt_update();
}