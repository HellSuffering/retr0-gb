#include "Maps_Animations.h"

#include "Maps_Tilesets.h"

/** Animations Tileset 0**/

// Waterfall anim
BackgroundAnim BackgroundAnim_WaterFall = BACKGROUNDANIM_PARAMS(0, 1, MapsTileset0, 16, 10);

// Water splash anim
BackgroundAnim BackgroundAnim_WaterSplash = BACKGROUNDANIM_PARAMS(2, 3, MapsTileset0, 16, 30);

// Water idle anim 0
BackgroundAnim BackgroundAnim_WaterIlde0 = BACKGROUNDANIM_PARAMS(9, 10, MapsTileset0, 16, 60);

// Water idle anim 1
BackgroundAnim BackgroundAnim_WaterIlde1 = BACKGROUNDANIM_PARAMS(9, 11, MapsTileset0, 16, 90);

/** Animations Tileset 1 **/

// Fire place anim
BackgroundAnim BackgroundAnim_Fireplace = BACKGROUNDANIM_PARAMS(0, 1, MapsTileset1, 16, 10);