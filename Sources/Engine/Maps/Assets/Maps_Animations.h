#ifndef DEF_MAPS_ANIMATIONS

#define DEF_MAPS_ANIMATIONS

#include "../BackgroundAnim.h"
#include "Maps.h"

/** Animations Tileset 0**/

// Waterfall anim
extern BackgroundAnim BackgroundAnim_WaterFall;

// Water splash anim
extern BackgroundAnim BackgroundAnim_WaterSplash;

// Water idle anim 0
extern BackgroundAnim BackgroundAnim_WaterIlde0;

// Water idle anim 1
extern BackgroundAnim BackgroundAnim_WaterIlde1;

/** Animations Tileset 1 **/

// Fire place anim
extern BackgroundAnim BackgroundAnim_Fireplace;

#endif