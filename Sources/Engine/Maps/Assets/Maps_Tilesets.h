#ifndef DEF_MAPS_TILESETS

#define DEF_MAPS_TILESETS

/** TILESET 0 **/

#define TILESET0_NBTILE 41

// Array of all tiles
extern const unsigned char MapsTileset0[];

/** TILESET 1 **/
#define TILESET1_NBTILE 19

// Array of all tiles
extern const unsigned char MapsTileset1[];

#endif