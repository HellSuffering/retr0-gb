#ifndef DEF_MAPS

#define DEF_MAPS

#include "../MapsCallbacks.h"
#include "Maps_Tilesets.h"

/** General Functions **/

/*
* Get collision map corresponding to specified map ID and return the collision data at specified index
*/
unsigned char Maps_GetCollisionDataAt(unsigned int _Index);

/*
* Check if specified array contains this tile
*/
static unsigned char Maps_IsCollidingTile(unsigned char _Tile, unsigned char* _CollidingTileArray, unsigned char _NbCollidingTile);

/** Map 16x16 **/

#define MAP16x16_WIDTH 32
#define MAP16x16_HEIGHT 32
#define MAP16x16_NBTILE 1024
#define MAP16x16_ID 0
#define MAP16x16_LOAD_PARAMS MAP16x16_ID, Map16x16_Data, MapsTileset0, TILESET0_NBTILE, MAP16x16_WIDTH, MAP16x16_HEIGHT, MapsCallbacks_OnMap16x16Loaded
#define MAP16x16_ISCOLLIDINGTILE(_Tile) _Tile == 0 || _Tile == 1 || _Tile == 2 || _Tile == 3 || _Tile == 9 || _Tile == 10 || _Tile == 11 || _Tile == 12 || _Tile == 13 || _Tile == 14 || _Tile == 15 || _Tile == 16 || _Tile == 17 || _Tile == 19 || _Tile == 20 || _Tile == 21 || _Tile == 22 || _Tile == 23 || _Tile == 24 || _Tile == 25 || _Tile == 26 || _Tile == 27 || _Tile == 28 || _Tile == 29 || _Tile == 30 || _Tile == 31 || _Tile == 32 || _Tile == 35 || _Tile == 36 || _Tile == 37 || _Tile == 38

// Map data
extern const char Map16x16_Data[];

/** Map ROOM 16x16 **/

#define MAPROOM16x16_WIDTH 20
#define MAPROOM16x16_HEIGHT 18
#define MAPROOM16x16_NBTILE 360
#define MAPROOM16x16_ID 1
#define MAPROOM16x16_LOAD_PARAMS MAPROOM16x16_ID, MapRoom16x16_Data, MapsTileset1, TILESET1_NBTILE, MAPROOM16x16_WIDTH, MAPROOM16x16_HEIGHT, MapsCallbacks_OnMapRoom16x16Loaded
#define MAPROOM16x16_ISCOLLIDINGTILE(_Tile) _Tile == 0 || _Tile == 1 || _Tile == 2 || _Tile == 3 || _Tile == 4 || _Tile == 7 || _Tile == 8 || _Tile == 9 || _Tile == 10 || _Tile == 11 || _Tile == 12 || _Tile == 13 || _Tile == 14 || _Tile == 15 || _Tile == 16

// Map data
extern const char MapRoom16x16_Data[];

/** Map Test Scroll X Axis **/

#define MapTestScrollXWidth 128
#define MapTestScrollXHeight 18
#define MapTestScrollX_ID 2
#define MapTestScrollX_LOAD_PARAMS MapTestScrollX_ID, MapTestScrollX, Map16x16_GetTileSet(), Map16x16_GetTilesetNbTiles(), MapTestScrollXWidth, MapTestScrollXHeight, MapsCallbacks_OnMapTestScrollXLoaded
#define MapTestScrollX_ISCOLLIDINGTILE(_Tile) _Tile == 14

extern const unsigned char MapTestScrollX[];

#endif