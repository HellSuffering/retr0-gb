#include "MapsCallbacks.h"

#include "Assets/Maps.h"
#include "Assets/Maps_Animations.h"

#include "../Sprites/Assets/Sprites_Tilesets.h"

#include "../Movables/Movable.h"
#include "../Game/Game.h"
#include "../Fade/Fade.h"
#include "../Inputs/InputManager.h"

/*
* Called when map 16x16 is loaded
*/
void MapsCallbacks_OnMap16x16Loaded(MapLoadedParameters* _CustomParameters)
{
    // Local variables
    Movable *playerMovable, *enemyMovable, *noCollisionMovable, *testLayerMovable, *pickableCoinMovable, *pickableLiveMovable, *doorMap1Movable;
    Box playerBox = {40,26,16,16};
    Box enemyBox = { 204, 80, 16, 16};
    Box noCollisionBox = {210, 115, 16, 16};
    Box testLayerBox = {70, 40, 16, 16};
    Box pickableCoinBox = {45, 200, 8, 8};
    Box pickableLiveBox = {50, 60, 8, 8};
    Box doorMap1Box = {184, 48, 16, 8};

    SPRITES_8x16;

    // Override player position
    if (_CustomParameters != NULL && _CustomParameters->isActive)
        Box_SetPosition(&playerBox, &_CustomParameters->playerPosition);

    // Load the 'sprites' tiles into sprite memory
	Sprite_LoadSpriteTileset(Sprites_Tileset16x16, SPRITES_TILESET16x16_NBTILE);

    // Create player movable
    playerMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PLAYER_LEFT, SPRITES_TILESET16x16_PLAYER_RIGHT, &playerBox, COLLISIONMODE_COLLISION, TAG_PLAYER, LAYER_0, LAYERMASK_0_ONLY, SCROLLRULE_SCROLLREFERENCE, TRUE);

    // Init other movable
    enemyMovable = Movable_CreateMovable(SPRITES_TILESET16x16_ENEMY_LEFT, SPRITES_TILESET16x16_ENEMY_RIGHT, &enemyBox, COLLISIONMODE_COLLISION, TAG_DEFAULT, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    // Init no collision movable
    noCollisionMovable = Movable_CreateMovable(SPRITES_TILESET16x16_GHOST_LEFT_0, SPRITES_TILESET16x16_GHOST_RIGHT_0, &noCollisionBox, COLLISIONMODE_NOCOLLISION, TAG_DEFAULT, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (noCollisionMovable != NULL)
        Movable_SetSpriteAnim(noCollisionMovable, &SpriteAnim_Ghost16x16_Left, &SpriteAnim_Ghost16x16_Right);

    // Init test layer movable
    testLayerMovable = Movable_CreateMovable(SPRITES_TILESET16x16_GHOST_LEFT_0, SPRITES_TILESET16x16_GHOST_RIGHT_0, &testLayerBox, COLLISIONMODE_COLLISION, TAG_DEFAULT, LAYER_1, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (testLayerMovable != NULL)
        Movable_SetSpriteAnim(testLayerMovable, &SpriteAnim_Ghost16x16_Left, &SpriteAnim_Ghost16x16_Right);

    // Init test pickable coin movable
    pickableCoinMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PICKUP_COIN, SPRITES_NOTILE, &pickableCoinBox, COLLISIONMODE_OVERLAP, TAG_PICKABLE_COIN, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (pickableCoinMovable != NULL)
        pickableCoinMovable->onOverlap = CollisionCallback_OnPickablePicked;

    // Init test pickable live movable
    pickableLiveMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PICKUP_LIVE, SPRITES_NOTILE, &pickableLiveBox, COLLISIONMODE_OVERLAP, TAG_PICKABLE_LIVE, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (pickableLiveMovable != NULL)
        pickableLiveMovable->onOverlap = CollisionCallback_OnPickablePicked;

    // Init door map 1 movable
    doorMap1Movable = Movable_CreateMovable(SPRITES_NOTILE, SPRITES_NOTILE, &doorMap1Box, COLLISIONMODE_OVERLAP, TAG_DEFAULT, LAYER_0, LAYERMASK_0_ONLY, SCROLLRULE_CANSCROLL, TRUE);

    if (doorMap1Movable != NULL)
        doorMap1Movable->onOverlap = CollisionCallback_TriggerLoadMap_Room16x16;

    // Bind map update callback
    Game_BindMapUpdateCallback(MapsCallbacks_OnMap16x16Update);

    // Fade screen
    wait_vbl_done();
    FADE_BLACKTOSTANDARD(30);
    InputManager_SetInputEnabled(TRUE);
}

/*
* Called when map room 16x16 is loaded
*/
void MapsCallbacks_OnMapRoom16x16Loaded(MapLoadedParameters* _CustomParameters)
{
    // Local variables
    Movable *playerMovable, *pickableCoinMovable, *pickableLiveMovable, *doorMap0Movable;
    Box playerBox = {72,128,16,16};
    Box pickableCoinBox = {30, 30, 8, 8};
    Box pickableLiveBox = {50, 60, 8, 8};
    Box doorMap0Box = {72, 136, 16, 8};

    SPRITES_8x16;

    // Load the 'sprites' tiles into sprite memory
	Sprite_LoadSpriteTileset(Sprites_Tileset16x16, SPRITES_TILESET16x16_NBTILE);

    // Create player movable
    playerMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PLAYER_LEFT, SPRITES_TILESET16x16_PLAYER_RIGHT, &playerBox, COLLISIONMODE_COLLISION, TAG_PLAYER, LAYER_0, LAYERMASK_0_ONLY, SCROLLRULE_SCROLLREFERENCE, TRUE);

    // Init test pickable coin movable
    pickableCoinMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PICKUP_COIN, SPRITES_NOTILE, &pickableCoinBox, COLLISIONMODE_OVERLAP, TAG_PICKABLE_COIN, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (pickableCoinMovable != NULL)
        pickableCoinMovable->onOverlap = CollisionCallback_OnPickablePicked;

    // Init test pickable live movable
    pickableLiveMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PICKUP_LIVE, SPRITES_NOTILE, &pickableLiveBox, COLLISIONMODE_OVERLAP, TAG_PICKABLE_LIVE, LAYER_0, LAYERMASK_ALL, SCROLLRULE_CANSCROLL, TRUE);

    if (pickableLiveMovable != NULL)
        pickableLiveMovable->onOverlap = CollisionCallback_OnPickablePicked;

    // Init door map 0 movable
    doorMap0Movable = Movable_CreateMovable(SPRITES_NOTILE, SPRITES_NOTILE, &doorMap0Box, COLLISIONMODE_OVERLAP, TAG_DEFAULT, LAYER_0, LAYERMASK_0_ONLY, SCROLLRULE_CANSCROLL, TRUE);

    if (doorMap0Movable != NULL)
        doorMap0Movable->onOverlap = CollisionCallback_LoadOutDoorMap;

    // Bind map update callback
    Game_BindMapUpdateCallback(MapsCallbacks_OnMapRoom16x16Update);

    // Fade screen
    wait_vbl_done();
    FADE_BLACKTOSTANDARD(30);
    InputManager_SetInputEnabled(TRUE);
}

/*
* Called at map update. Play background animations for map 16x16
*/
void MapsCallbacks_OnMap16x16Update()
{
    BackgroundAnim_UpdateBackgroundAnim(&BackgroundAnim_WaterFall);
    BackgroundAnim_UpdateBackgroundAnim(&BackgroundAnim_WaterSplash);
    BackgroundAnim_UpdateBackgroundAnim(&BackgroundAnim_WaterIlde0);
    BackgroundAnim_UpdateBackgroundAnim(&BackgroundAnim_WaterIlde1);
}

/*
* Called at map update. Play background animations for map room 16x16
*/
void MapsCallbacks_OnMapRoom16x16Update()
{
    BackgroundAnim_UpdateBackgroundAnim(&BackgroundAnim_Fireplace);
}

void MapsCallbacks_OnMapTestScrollXLoaded()
{
        // Local variables
    Movable *playerMovable;
    Box playerBox = {72,128,16,16};

    SPRITES_8x16;

    // Load the 'sprites' tiles into sprite memory
	Sprite_LoadSpriteTileset(Sprites_Tileset16x16, SPRITES_TILESET16x16_NBTILE);

    // Create player movable
    playerMovable = Movable_CreateMovable(SPRITES_TILESET16x16_PLAYER_LEFT, SPRITES_TILESET16x16_PLAYER_RIGHT, &playerBox, COLLISIONMODE_COLLISION, TAG_PLAYER, LAYER_0, LAYERMASK_0_ONLY, SCROLLRULE_SCROLLREFERENCE, FALSE);

    // Fade screen
    wait_vbl_done();
    FADE_BLACKTOSTANDARD(30);
    InputManager_SetInputEnabled(TRUE);
}