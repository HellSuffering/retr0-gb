#ifndef DEF_MAPSCALLBACKS

#define DEF_MAPSCALLBACKS

#include "MapLoader.h"

/** Callbacks **/

/*
* Called when map 16x16 is loaded
*/
void MapsCallbacks_OnMap16x16Loaded(MapLoadedParameters* _CustomParameters);

/*
* Called when map room 16x16 is loaded
*/
void MapsCallbacks_OnMapRoom16x16Loaded(MapLoadedParameters* _CustomParameters);

/*
* Called at map update. Play background animations for map 16x16
*/
void MapsCallbacks_OnMap16x16Update();

/*
* Called at map update. Play background animations for map room 16x16
*/
void MapsCallbacks_OnMapRoom16x16Update();

void MapsCallbacks_OnMapTestScrollXLoaded();

#endif