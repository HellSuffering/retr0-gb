#include "MapLoader.h"

#include "../Scroll/ScrollManager.h"
#include "../Movables/Movable.h"

/*
* Load pending map if any
*/
void MapLoader_LoadPendingMap()
{
    if (s_PendingLoadMapData.isPending)
    {
        s_PendingLoadMapData.isPending = FALSE;
        MapLoader_LoadMapData(s_PendingLoadMapData.mapID, s_PendingLoadMapData.mapData, s_PendingLoadMapData.tileSet, s_PendingLoadMapData.nbTiles, s_PendingLoadMapData.mapWidth, s_PendingLoadMapData.mapHeight);
    }
}

/*
* Request a load map data setting pending load map data
*/
void MapLoader_RequestLoadMapData(UINT8 _MapID, UINT8* _Data, UINT8* _TileSet, UINT8 _NbTiles, UINT8 _Width, UINT8 _Height, void (*_Callback)(MapLoadedParameters*), MapLoadedParameters* _CustomCallbackParameters)
{
    s_PendingLoadMapData.isPending = TRUE;
    s_PendingLoadMapData.mapID = _MapID;
    s_PendingLoadMapData.mapData = _Data;
    s_PendingLoadMapData.tileSet = _TileSet;
    s_PendingLoadMapData.nbTiles = _NbTiles;
    s_PendingLoadMapData.mapWidth = _Width;
    s_PendingLoadMapData.mapHeight = _Height;
    s_PendingLoadMapData.onMapLoaded = _Callback;

    if (_CustomCallbackParameters != NULL)
    {
        s_PendingLoadMapData.mapLoadedParameters.isActive = TRUE;
        MapLoader_CopyMapLoadedParameters(_CustomCallbackParameters, &s_PendingLoadMapData.mapLoadedParameters);
    }
    else
    {
        s_PendingLoadMapData.mapLoadedParameters.isActive = FALSE;
    }
}

/*
* Get current map ID
*/
UINT8 MapLoader_GetCurrentMapID()
{
    return s_CurrentMapID;
}

/*
* Get loaded map data
*/
PendingLoadMapData* MapLoader_GetLoadedMapData()
{
    return &s_LoadedMapData;
}

/*
* Get collision map corresponding to specified map ID and return the collision data at specified index
*/
unsigned int MapLoader_GetCollisionDataAt(unsigned int _Index)
{
    return Maps_GetCollisionDataAt(_Index);
}

/*
* Create map from all passed data and load all its tiles into background memory
*/
static void MapLoader_LoadMapData(UINT8 _MapID, UINT8* _Data, UINT8* _TileSet, UINT8 _NbTiles, UINT8 _Width, UINT8 _Height)
{
    // Load tileset into background memory
    set_bkg_data(0, _NbTiles, _TileSet);
    
    // Create map
    set_bkg_tiles(0, 0, _Width, _Height, _Data);

    // Set current map ID
    s_CurrentMapID = _MapID;

    // Reset scroll offset
    ScrollManager_ResetScroll();

    // Set values for loaded map data
    s_LoadedMapData.isPending = FALSE;
    s_LoadedMapData.mapData = s_PendingLoadMapData.mapData;
    s_LoadedMapData.mapHeight = s_PendingLoadMapData.mapHeight;
    s_LoadedMapData.mapID = s_PendingLoadMapData.mapID;
    s_LoadedMapData.mapWidth = s_PendingLoadMapData.mapWidth;
    s_LoadedMapData.nbTiles = s_PendingLoadMapData.nbTiles;
    s_LoadedMapData.tileSet = s_PendingLoadMapData.tileSet;
    s_LoadedMapData.onMapLoaded = s_PendingLoadMapData.onMapLoaded;

    if (s_PendingLoadMapData.mapLoadedParameters.isActive)
    {
        s_LoadedMapData.mapLoadedParameters.isActive = TRUE;
        MapLoader_CopyMapLoadedParameters(&s_PendingLoadMapData.mapLoadedParameters, &s_LoadedMapData.mapLoadedParameters);
    }
    else
    {
        s_LoadedMapData.mapLoadedParameters.isActive = FALSE;
    }

    // Destroy all movables
    Movable_DestroyAll();

    // Callback
    if (s_LoadedMapData.onMapLoaded != NULL)
        (*s_LoadedMapData.onMapLoaded)(&s_LoadedMapData.mapLoadedParameters);
}

/*
* Copy data from specified map loaded parameters to an other
*/
static void MapLoader_CopyMapLoadedParameters(MapLoadedParameters* _Source, MapLoadedParameters* _Destination)
{
    if (_Source != NULL && _Destination != NULL)
    {
        Vector_CopyTo(&_Source->playerPosition, &_Destination->playerPosition);
    }
}