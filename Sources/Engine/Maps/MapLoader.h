#ifndef DEF_MAPLOADER

#define DEF_MAPLOADER

#include <gb/gb.h>
#include "../Math/Vector.h"

/** Structs **/

// Struct used to pass custom parameters in map loaded callback. Modify it to add your own data
struct MapLoadedParameters
{
    // Player movable position after map loading
    Vector playerPosition;

    // Is this parameters active or not
    UINT8 isActive;
};
typedef struct MapLoadedParameters MapLoadedParameters;

// Cannot be included before MapLoaderParameters definition
#include "Assets/Maps.h"

// Struct used to define all data for a pending load map
struct PendingLoadMapData
{
    // Is load map pending
    UINT8 isPending;

    // Map ID to load
    UINT8 mapID;

    // Map data to load
    UINT8* mapData;

    // Tileset to use for the map
    UINT8* tileSet;

    // Nb tiles in tileset to use
    UINT8 nbTiles;

    // Map width
    UINT8 mapWidth;

    // Map height
    UINT8 mapHeight;

    // Pointer on function called when a map is loaded
    void (*onMapLoaded)(MapLoadedParameters*);

    // Custom parameters to pass to map loaded callback
    MapLoadedParameters mapLoadedParameters;
};
typedef struct PendingLoadMapData PendingLoadMapData;

/** Exposed Functions **/

/*
* Load pending map if any
*/
void MapLoader_LoadPendingMap();

/*
* Request a load map data setting pending load map data
*/
void MapLoader_RequestLoadMapData(UINT8 _MapID, UINT8* _Data, UINT8* _TileSet, UINT8 _NbTiles, UINT8 _Width, UINT8 _Height, void (*_Callback)(MapLoadedParameters*), MapLoadedParameters* _CustomCallbackParameters);

/*
* Get current map ID
*/
UINT8 MapLoader_GetCurrentMapID();

/*
* Get loaded map data
*/
PendingLoadMapData* MapLoader_GetLoadedMapData();

/*
* Get collision map corresponding to specified map ID and return the collision data at specified index
*/
unsigned int MapLoader_GetCollisionDataAt(unsigned int _Index);

/** Static Functions **/

/*
* Create map from all passed data and load all its tiles into background memory
*/
static void MapLoader_LoadMapData(UINT8 _MapID, UINT8* _Data, UINT8* _TileSet, UINT8 _NbTiles, UINT8 _Width, UINT8 _Height);

/*
* Copy data from specified map loaded parameters to an other
*/
static void MapLoader_CopyMapLoadedParameters(MapLoadedParameters* _Source, MapLoadedParameters* _Destination);

/** Static Variables **/

// ID of current map
static UINT8 s_CurrentMapID = 0;

// Pending load map data
static PendingLoadMapData s_PendingLoadMapData;

// Loaded map data
static PendingLoadMapData s_LoadedMapData;

#endif