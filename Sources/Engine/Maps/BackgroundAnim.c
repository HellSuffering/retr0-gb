#include "BackgroundAnim.h"
#include "Assets/Maps.h"

/*
* Update background anim. Check which background anim frame to make active and update background tile if needed
*/
void BackgroundAnim_UpdateBackgroundAnim(BackgroundAnim* _BackgroundAnim)
{
    if (_BackgroundAnim != NULL)
    {
        if (_BackgroundAnim->nbFrameElpased >= _BackgroundAnim->nbFrameToWait)
        {
            if (!_BackgroundAnim->hasBeenSwapped)
            {
                // Swap the tiles
                set_bkg_data(_BackgroundAnim->swapTilePos0, 1, _BackgroundAnim->tileset + (_BackgroundAnim->tileSize * _BackgroundAnim->swapTilePos1));
                set_bkg_data(_BackgroundAnim->swapTilePos1, 1, _BackgroundAnim->tileset + (_BackgroundAnim->tileSize * _BackgroundAnim->swapTilePos0));
            }
            else
            {
                // Restore original tiles
                set_bkg_data(_BackgroundAnim->swapTilePos0, 1, _BackgroundAnim->tileset + (_BackgroundAnim->tileSize * _BackgroundAnim->swapTilePos0));
                set_bkg_data(_BackgroundAnim->swapTilePos1, 1, _BackgroundAnim->tileset + (_BackgroundAnim->tileSize * _BackgroundAnim->swapTilePos1));
            }

            _BackgroundAnim->hasBeenSwapped = !_BackgroundAnim->hasBeenSwapped;
            _BackgroundAnim->nbFrameElpased = 0;
        }
        else
        {
            // Update nb frame elapsed
            _BackgroundAnim->nbFrameElpased++;
        }
    }
}