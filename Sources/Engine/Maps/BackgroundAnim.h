#ifndef DEF_BACKGROUNDANIM

#define DEF_BACKGROUNDANIM

#include <gb/gb.h>

#define BACKGROUNDANIM_NBFRAMES 2

#define BACKGROUNDANIM_PARAMS(_SwapTilePos0, _SwapTilePos1, _Tileset, _TileSize, _NbFrameToWait) {_SwapTilePos0, _SwapTilePos1, _Tileset, _TileSize, _NbFrameToWait, 0, FALSE}

/** Structs **/

/* 
* Struct used to group all data for a background animation
* BE CAREFULL, ANIMATED TILES CAN ONLY BE ON THE FIRST LINE IN VRAM VIEWER. 
* OTHERWISE VISUAL GLTICHES CAN HAPPEN (BGB BUG OR REAL GB BUG ?)
*/
struct BackgroundAnim
{
    // Position of tile to swap in background tiles VRAM
    UINT8 swapTilePos0;

    // Position of other tile to swap in background tiles VRAM
    UINT8 swapTilePos1;

    // Tileset to use to pick tile data
    unsigned char* tileset;

    // Size of tile in tileset
    UINT8 tileSize;

    // Number of frame to wait to swap tiles
    UINT8 nbFrameToWait;

    // Number of frame elapsed since last swap time
    UINT8 nbFrameElpased;

    // Has tiles already been swapped
    UINT8 hasBeenSwapped;
};
typedef struct BackgroundAnim BackgroundAnim;

#endif

/** Exposed Functions **/

/*
* Update background anim. Check which background anim frame to make active and update background tile if needed
*/
void BackgroundAnim_UpdateBackgroundAnim(BackgroundAnim* _BackgroundAnim);

//unsigned char BackgroundTiles[360];