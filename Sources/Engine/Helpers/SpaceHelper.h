#ifndef DEF_SPACEHELPER

#define DEF_SPACEHELPER

#include "../Math/Vector.h"

/** Exposed Functions **/

/*
* Convert a screen position in tile position (position of tile containing the screen position in tile space)
*/
void SpaceHelper_ScreenSpaceToTileSpace(Vector* _ScreenPosition, Vector* _OutTilePosition);

/*
* Convert a tile position (in tile space) in screen position (in screen space)
*/
void SpaceHelper_TileSpaceToScreenSpace(Vector* _TilePosition, Vector* _OutScreenPosition);

/*
* Is position (in screen space) in screen or out screen
*/
UINT8 SpaceHelper_IsPositionInScreen(Vector* _ScreenPosition);

#endif