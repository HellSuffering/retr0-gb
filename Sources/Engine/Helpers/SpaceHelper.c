#include "SpaceHelper.h"

/*
* Convert a screen position in tile position (position of tile containing the screen position in tile space)
*/
void SpaceHelper_ScreenSpaceToTileSpace(Vector* _ScreenPosition, Vector* _OutTilePosition)
{
    if (_ScreenPosition != NULL && _OutTilePosition != NULL)
    {
        _OutTilePosition->x = _ScreenPosition->x >> 3;
        _OutTilePosition->y = _ScreenPosition->y >> 3;
    }
}

/*
* Convert a tile position (in tile space) in screen position (in screen space)
*/
void SpaceHelper_TileSpaceToScreenSpace(Vector* _TilePosition, Vector* _OutScreenPosition)
{
    if (_TilePosition != NULL && _OutScreenPosition != NULL)
    {
        _OutScreenPosition->x = _TilePosition->x << 3;
        _OutScreenPosition->y = _TilePosition->y << 3;
    }
}

/*
* Is position (in screen space) in screen or out screen
*/
UINT8 SpaceHelper_IsPositionInScreen(Vector* _ScreenPosition)
{
    if (_ScreenPosition != NULL)
    {
        return ((UINT8)_ScreenPosition->x < 168 || (UINT8)_ScreenPosition->x > 247) && ((UINT8)_ScreenPosition->y < 160 || (UINT8)_ScreenPosition->y > 247);
        //return ((UINT8)_ScreenPosition->x < 168 && _ScreenPosition->x > -8) && ((UINT8)_ScreenPosition->y < 160 && _ScreenPosition->y > -8);
    }

    return FALSE;
}