#include "Collision.h"

#include "../Tags_Layers/Layers.h"
#include "../Maps/MapLoader.h"
#include "../Scroll/ScrollManager.h"

/*
* Check collision between a movable and all other colliding elements (movable or map collision)
*/
UINT8 Collision_CheckCollisionForMovable(Movable* _Movable, Vector* _PositionToCheckMovableCollision, Vector* _PositionToCheckMapCollision, CollisionResult* _OutCollisionResult)
{
    // Local variable
    UINT8 isColliding = FALSE;

    if (_Movable != NULL)
    {
        // Ensure collision result is not null
        if (_OutCollisionResult == NULL)
            return FALSE;

        // Check collision with other movables
        isColliding = Collision_CheckMovableCollision(_Movable, _PositionToCheckMovableCollision, _OutCollisionResult);
        
        // Check collision with map
        if (!isColliding)
            isColliding = Collision_CheckMapCollision(_Movable, _PositionToCheckMapCollision, _OutCollisionResult);
    }

    return isColliding;
}

/*
* Check collision between 2 movables
*/
UINT8 Collision_CheckCollisionBetweenMovables(Movable* _MovableA, Vector* _PositionToCheck, Movable* _MovableB)
{
    // Local variables
    Box updatedBox;

    if (_MovableA != NULL && _MovableB != NULL && _MovableA != _MovableB)
    {
        // Create box from movable A box with check position set on it
        Box_Copy(&_MovableA->box, &updatedBox);
        Box_SetPosition(&updatedBox, _PositionToCheck);

        // Check box intersect
        return Box_Intersect(&updatedBox, &_MovableB->box);
    }

    return FALSE;
}

/*
* Check collision between a movable and all other movables
*/
static UINT8 Collision_CheckMovableCollision(Movable* _Movable, Vector* _PositionToCheck, CollisionResult* _OutCollisionResult)
{
    // Local variables
    UINT8 i;
    Movable* otherMovable = Movable_GetMovables();

    // Check collision with all other movables
    for (i = 0; i < MOVABLE_LIMIT; i++)
    {
        if (otherMovable != NULL && otherMovable->enabled && otherMovable != _Movable && otherMovable->collisionMode != COLLISIONMODE_NOCOLLISION)
        {
            if (LAYERMASK_CONTAINS(_Movable->layerMask, otherMovable->layer) && LAYERMASK_CONTAINS(otherMovable->layerMask, _Movable->layer))
            {
                if (Collision_CheckCollisionBetweenMovables(_Movable, _PositionToCheck, otherMovable))
                {
                    if (_OutCollisionResult != NULL)
                    {
                        // Fill collision result
                        _OutCollisionResult->otherMovable = otherMovable;
                        _OutCollisionResult->isBlocking = (otherMovable->collisionMode == COLLISIONMODE_COLLISION);

                        // Call the movable callback if any
                        if (!_OutCollisionResult->isBlocking)
                        {
                            if (_Movable->onOverlap != NULL)
                                (*_Movable->onOverlap)(_Movable, otherMovable);

                            if (otherMovable->onOverlap != NULL)
                                (*otherMovable->onOverlap)(otherMovable, _Movable);
                        }

                        return TRUE;
                    }
                }
            }
        }

        otherMovable++;
    }

    return FALSE;
}

/*
* Check collision between a movable and map collisions
*/
static UINT8 Collision_CheckMapCollision(Movable* _Movable, Vector* _PositionToCheck, CollisionResult* _OutCollisionResult)
{
    // Local variables
    int baseX, baseY;
    int x, y;
    Vector scrollOffset, movablePosition, translation;
    UINT8 mapWidth = MapLoader_GetLoadedMapData()->mapWidth;
    UINT8 boxWidthTileSpace, boxHeightTileSpace;
    UINT8 w, h;

    // Get background tiles on movable box
    if (_Movable != NULL)
    {
        // Get translation
        Box_GetPosition(&_Movable->box, &movablePosition);
        Vector_Subtract(_PositionToCheck, &movablePosition, &translation);

        // Get scroll offset (needed to get the real tile position in map space)
        Vector_CopyTo(ScrollManager_GetScrollOffset(), &scrollOffset);

        // Get base values
        boxWidthTileSpace = _Movable->box.width >> 3;
        boxHeightTileSpace = _Movable->box.height >> 3;
        baseX = ((_PositionToCheck->x + scrollOffset.x) >> 3);
        baseY = ((_PositionToCheck->y + scrollOffset.y) >> 3);

        if (!_Movable->constrainOnTiles)
        {
            boxWidthTileSpace++;
            boxHeightTileSpace++;
        }

        // Get searched map tile position from player position (used to find tile index)
        if (translation.x != 0)
        {
            for (h = 0; h < boxHeightTileSpace; h++)
            {
                if (!_Movable->constrainOnTiles)
                {
                    if (h == 0)
                        baseY = ((_PositionToCheck->y + scrollOffset.y + 1) >> 3);
                    else if (h == boxHeightTileSpace - 1)
                        baseY = ((_PositionToCheck->y + scrollOffset.y - 1) >> 3);
                }

                x = baseX;
                y = baseY + h;

                if (Maps_GetCollisionDataAt(mapWidth * y + x) == TRUE)
                {
                    _OutCollisionResult->isBlocking = TRUE;
                    return TRUE;
                }
            }
        }

        if (translation.y != 0)
        {
            for (w = 0; w < boxWidthTileSpace; w++)
            {
                if (!_Movable->constrainOnTiles)
                {
                    if (w == 0)
                        baseX = ((_PositionToCheck->x + scrollOffset.x + 1) >> 3);
                    else if (w == boxWidthTileSpace - 1)
                        baseX = ((_PositionToCheck->x + scrollOffset.x - 1) >> 3);
                }

                x = baseX + w;
                y = baseY;

                if (Maps_GetCollisionDataAt(mapWidth * y + x) == TRUE)
                {
                    _OutCollisionResult->isBlocking = TRUE;
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}