#ifndef DEF_COLLISIONCALLBACKS

#define DEF_COLLISIONCALLBACKS

#include "Collision.h"

/** Example Functions **/

/*
* Called on movable overlap. Destroy 'other' movable and play sound depending on tag
*/
void CollisionCallback_OnPickablePicked(Movable* _Self, Movable* _Other);

/*
* Called on movable overlap. Load map 16x16
*/
void CollisionCallback_LoadOutDoorMap(Movable* _Self, Movable* _Other);

/*
* Called on movable overlap. Load map room 16x16
*/
void CollisionCallback_TriggerLoadMap_Room16x16(Movable* _Self, Movable* _Other);

#endif