#include "CollisionCallbacks.h"

#include "../Maps/MapLoader.h"
#include "../Fade/Fade.h"
#include "../Inputs/InputManager.h"

/*
* Called on movable overlap. Destroy 'other' movable and play sound depending on tag
*/
void CollisionCallback_OnPickablePicked(Movable* _Self, Movable* _Other)
{
    if (_Self != NULL)
    {
        if (_Self->tag == TAG_PICKABLE_COIN)
        {
            // Audio
            NR52_REG = 0x80;
            NR51_REG = 0x11;
            NR50_REG = 0x77;

            NR10_REG = 0x0D;
            NR11_REG = 0x11;
            NR12_REG = 0xF2;
            NR13_REG = 0x01;
            NR14_REG = 0x86;
        }
        else if (_Self->tag == TAG_PICKABLE_LIVE)
        {
            // Audio
            NR52_REG = 0x80;
            NR51_REG = 0x11;
            NR50_REG = 0x77;

            NR10_REG = 0x1E;
            NR11_REG = 0x10;
            NR12_REG = 0xF3;
            NR13_REG = 0x00;
            NR14_REG = 0x87;
        }

        // Destroy movable
        Movable_Destroy(_Self);
    }
}

/*
* Called on movable overlap. Load map 16x16
*/
void CollisionCallback_LoadOutDoorMap(Movable* _Self, Movable* _Other)
{
    // Local variables
    MapLoadedParameters customParams = {{184,64}};

    // Request load map
    MapLoader_RequestLoadMapData(MAP16x16_LOAD_PARAMS, &customParams);

    // Fade
    InputManager_SetInputEnabled(FALSE);
    FADE_STANDARDTOBLACK(30);
}

/*
* Called on movable overlap. Load map room 16x16
*/
void CollisionCallback_TriggerLoadMap_Room16x16(Movable* _Self, Movable* _Other)
{
    // Request load map data
    MapLoader_RequestLoadMapData(MAPROOM16x16_LOAD_PARAMS, NULL);

    // Fade 
    InputManager_SetInputEnabled(FALSE);
    FADE_STANDARDTOBLACK(30);
}