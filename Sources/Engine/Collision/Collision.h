#ifndef DEF_COLLISION

#define DEF_COLLISION

#include "../Movables/Movable.h"
#include "../Tags_Layers/Tags.h"
#include "../Tags_Layers/Layers.h"
#include "CollisionCallbacks.h"

/** Structs **/

// Struct used to group all data for a collision. Easier to return from check collision functions
struct CollisionResult
{
    // Movable hit by movable checking collision
    Movable* otherMovable;

    // Is collision blocking (can be an overlap)
    UINT8 isBlocking;
};
typedef struct CollisionResult CollisionResult;

/** Exposed Functions **/

/*
* Check collision between a movable and all other colliding elements (movable or map collision)
*/
UINT8 Collision_CheckCollisionForMovable(Movable* _Movable, Vector* _PositionToCheckMovableCollision, Vector* _PositionToCheckMapCollision, CollisionResult* _OutCollisionResult);

/*
* Check collision between 2 movables
*/
UINT8 Collision_CheckCollisionBetweenMovables(Movable* _MovableA, Vector* _PositionToCheck, Movable* _MovableB);

/** Static Functions **/

/*
* Check collision between a movable and all other movables
*/
static UINT8 Collision_CheckMovableCollision(Movable* _Movable, Vector* _PositionToCheck, CollisionResult* _OutCollisionResult);

/*
* Check collision between a movable and map collisions
*/
static UINT8 Collision_CheckMapCollision(Movable* _Movable, Vector* _PositionToCheck, CollisionResult* _OutCollisionResult);

#endif