#ifndef DEF_FADE

#define DEF_FADE

// Fade from standard palette to white
#define FADE_STANDARDTOWHITE(_FadeDelay)    BGP_REG = OBP0_REG = OBP1_REG = 0xE4U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0x90U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0x40U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0x00U

// Fade from white to standard palette
#define FADE_WHITETOSTANDARD(_FadeDelay)    BGP_REG = OBP0_REG = OBP1_REG = 0x00U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0x40U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0x90U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0xE4U

// Fade from standard palette to black
#define FADE_STANDARDTOBLACK(_FadeDelay)    BGP_REG = OBP0_REG = OBP1_REG = 0xE4U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0XF9U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0xFEU; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0xFFU  
                                    
// Fade from black to standard palette
#define FADE_BLACKTOSTANDARD(_FadeDelay)    BGP_REG = OBP0_REG = OBP1_REG = 0xFFU; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0xFEU; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0XF9U; \
                                            delay(_FadeDelay); \
                                            BGP_REG = OBP0_REG = OBP1_REG = 0xE4U                                 

#endif