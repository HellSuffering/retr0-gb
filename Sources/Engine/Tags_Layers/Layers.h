#ifndef DEF_LAYERS

#define DEF_LAYERS

/** LAYERS **/
#define LAYER_0 1 // 2^0, bit 0
#define LAYER_1 2 // 2^1, bit 1
#define LAYER_2 4 // 2^2, bit 2
#define LAYER_3 8 // 2^3, bit 3
#define LAYER_4 16 // 2^3, bit 4
#define LAYER_5 32 // 2^3, bit 5
#define LAYER_6 64 // 2^3, bit 6
#define LAYER_7 128 // 2^3, bit 7

/** LAYERMASKS **/
#define LAYERMASK_ALL (LAYER_0 | LAYER_1 | LAYER_2 | LAYER_3 | LAYER_4 | LAYER_5 | LAYER_6 | LAYER_7)

/** MACROS **/
#define LAYERMASK_CONTAINS(_LAYERMASK, _LAYER) ((_LAYERMASK) & _LAYER) == _LAYER

// Enter custom layer mask here
#define LAYERMASK_0_ONLY LAYER_0

#endif