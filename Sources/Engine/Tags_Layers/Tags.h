#ifndef DEF_TAGS

#define DEF_TAGS

#define TAG_DEFAULT 0

// Enter custom tags as define here
#define TAG_PLAYER 1
#define TAG_PICKABLE_COIN 2
#define TAG_PICKABLE_LIVE 3

#define TAG_MAX 254

#endif