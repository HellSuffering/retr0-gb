#include "ScrollManager.h"

#include "../Maps/MapLoader.h"
#include "../Movables/Movable.h"

/*
* Make background and all scrollable movable scrolling in specified direction
*/
void ScrollManager_Scroll(Vector* _Direction)
{
    // Local variables
    UINT8 i;
    Vector movableTranslation;
    Movable* movable = Movable_GetMovables();
    unsigned char* tiles = NULL;

    if (_Direction != NULL)
    {
        // Scroll mode X / Y
        if (s_CurrentScrollMode == SCROLLMODE_PLATFORMER)
        {
            _Direction->y = 0;
        }

        // Make background scrolling
        scroll_bkg(_Direction->x, _Direction->y);

        // Make scrollable movables scrolling
        movableTranslation.x = - _Direction->x;
        movableTranslation.y = - _Direction->y;

        for (i = 0; i < MOVABLE_LIMIT; i++)
        {
            if (movable != NULL && movable->enabled && movable->scrollRule == SCROLLRULE_CANSCROLL)
            {
                Movable_Scroll(movable, &movableTranslation);
            }

            movable++;
        }

        // Update scroll offset
        Vector_Add(_Direction, &s_ScrollOffset, &s_ScrollOffset);
    }
}

/*
* Check if we can scroll in sepcified direction checking reference position on map bounds and desired scroll direction
*/
UINT8 ScrollManager_CanScroll(Vector* _ReferencePosition, Vector* _ScrollDirection)
{
    switch (s_CurrentScrollMode)
    {
        case SCROLLMODE_FREE:
            return ScrollManager_CanScroll_Free(_ReferencePosition, _ScrollDirection);

        case SCROLLMODE_PLATFORMER:
            return ScrollManager_CanScroll_Platformer(_ReferencePosition, _ScrollDirection);

        default:
            return TRUE;
    }
}

/*
* Add offset to desired scroll offset
*/
void ScrollManager_AddDesiredScrollOffset(Vector* _AddOffset)
{
    // Local variables
    unsigned char* tiles = NULL;
    UINT8 i;

    // Add offset to desired scroll offset
    Vector_Add(&s_DesiredScrollOffset, _AddOffset, &s_DesiredScrollOffset);

    // Scroll mode X > stream next column of tiles
    if (s_CurrentScrollMode == SCROLLMODE_PLATFORMER)
    {
        // Check a new tile enter in screen
        if (s_DesiredScrollOffset.x % 8 == 0)
        {
            // Update number of tiles scrolled
            s_NbTileScrolled++;

            // Get array of tiles
            tiles = MapLoader_GetLoadedMapData()->mapData + 20 + s_NbTileScrolled;

            // Replace each tile of the column
            for (i = 0; i < 18; i++)
            {
                // Replace tile
                set_bkg_tiles(((20 + s_NbTileScrolled) % 32), i, 1, 1, tiles);

                // Get next tile in map
                tiles += MapLoader_GetLoadedMapData()->mapWidth;
            }
        }
    }
}

/*
* Get scroll offset
*/
Vector* ScrollManager_GetScrollOffset()
{
    return &s_ScrollOffset;
}

/*
* Scroll until scroll offset become desired scroll offset
*/
Vector* ScrollManager_ScrollToDesiredOffset()
{
    // Local variables
    Vector scrollDirection;

    if (!Vector_Equals(&s_ScrollOffset, &s_DesiredScrollOffset))
    {
        // Get scroll direction
        Vector_Subtract(&s_DesiredScrollOffset, &s_ScrollOffset, &scrollDirection);
        Vector_Normalize(&scrollDirection);

        // Scroll
        ScrollManager_Scroll(&scrollDirection);
    }
}

/*
* Check scroll offset equals scroll desired offset
*/
UINT8 ScrollManager_IsAtDesiredOffset()
{
    return Vector_Equals(&s_ScrollOffset, &s_DesiredScrollOffset);
}

/*
* Reset scroll setting desired scroll offset, scroll offset and background position to zero vector
*/
void ScrollManager_ResetScroll()
{
    // Reset desired source offset to zero vector
    s_DesiredScrollOffset.x = 0;
    s_DesiredScrollOffset.y = 0;

    // Reset source offset to zero vector
    s_ScrollOffset.x = 0;
    s_ScrollOffset.y = 0;

    // Reset background position to zero
    move_bkg(0, 0);
}

/*
* Set current scroll mode
*/
void ScrollManager_SetScrollMode(EScrollMode _ScrollMode)
{
    s_CurrentScrollMode = _ScrollMode;
}

/*
* Check if we can scroll on X axis
*/
UINT8 ScrollManager_CanScroll_Platformer(Vector* _ReferencePosition, Vector* _ScrollDirection)
{
    // Local variables
    UINT16 x;
    UINT8 mapWidth = MapLoader_GetLoadedMapData()->mapWidth;

    if (mapWidth <= 20)
        return FALSE;
    
    if (_ReferencePosition != NULL && _ScrollDirection != NULL)
    {
        x = (_ReferencePosition->x >> 3); // n >> 3 == n / 8

        // Check we can scroll on X
        if (_ScrollDirection->x > 0)
        {
            if (x < 9 || s_NbTileScrolled > mapWidth - 21)
                return FALSE;
        }
        else
        {
            return FALSE;
        }
    }

    return TRUE;
}

/*
* Check if we can scroll in specified direction
*/
UINT8 ScrollManager_CanScroll_Free(Vector* _ReferencePosition, Vector* _ScrollDirection)
{
    // Local variables
    UINT8 x, y;
    UINT8 mapWidth = MapLoader_GetLoadedMapData()->mapWidth;
    UINT8 mapHeight = MapLoader_GetLoadedMapData()->mapHeight;

    if (mapWidth <= 20 && mapHeight <= 18)
        return FALSE;
    
    if (_ReferencePosition != NULL && _ScrollDirection != NULL)
    {
        // Snap reference position to tile to get tile position
        x = (_ReferencePosition->x >> 3) + (s_ScrollOffset.x >> 3); // n >> 3 == n / 8
        y = (_ReferencePosition->y >> 3) + (s_ScrollOffset.y >> 3); // n >> 3 == n / 8

        // Check we can scroll on X
        if (_ScrollDirection->x > 0)
        {
            if (x < 10 || (mapWidth - x) < 11)
                return FALSE;
        }
        else if (_ScrollDirection->x < 0)
        {
            if (s_ScrollOffset.x == 0 || x > (mapWidth - 12))
                return FALSE;
        }

        // Check we can scroll on Y
        if (_ScrollDirection->y > 0)
        {
            if (y < 9 || (mapHeight - y) < 10)
                return FALSE;
        }
        else if (_ScrollDirection->y < 0)
        {
            if (s_ScrollOffset.y == 0 || y > (mapHeight - 11))
                return FALSE;
        }
    }

    return TRUE;
}