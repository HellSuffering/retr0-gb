#ifndef DEF_SCROLLMANAGER

#define DEF_SCROLLMANAGER

#include <gb/gb.h>
#include "../Math/Vector.h"

/** Enums **/

// Define how to scroll
enum EScrollMode
{
    SCROLLMODE_FREE,        // Scroll on X and Y axis like any RPG. Map size limited to 32x32 max
    SCROLLMODE_PLATFORMER   // Scroll on right direction only like Super Mario Land. Map width limited to 128
};
typedef enum EScrollMode EScrollMode;

/** Exposed Functions **/

/*
* Make background and all scrollable movable scrolling in specified direction
*/
void ScrollManager_Scroll(Vector* _Direction);

/*
* Check if we can scroll in sepcified direction checking reference position on map bounds and desired scroll direction
*/
UINT8 ScrollManager_CanScroll(Vector* _ReferencePosition, Vector* _ScrollDirection);

/*
* Add offset to desired scroll offset
*/
void ScrollManager_AddDesiredScrollOffset(Vector* _AddOffset);

/*
* Get scroll offset
*/
Vector* ScrollManager_GetScrollOffset();

/*
* Scroll until scroll offset become desired scroll offset
*/
Vector* ScrollManager_ScrollToDesiredOffset();

/*
* Check scroll offset equals scroll desired offset
*/
UINT8 ScrollManager_IsAtDesiredOffset();

/*
* Reset scroll setting desired scroll offset, scroll offset and background position to zero vector
*/
void ScrollManager_ResetScroll();

/*
* Set current scroll mode
*/
void ScrollManager_SetScrollMode(EScrollMode _ScrollMode);

/** Static Methods **/

/*
* Check if we can scroll on X axis
*/
UINT8 ScrollManager_CanScroll_Platformer(Vector* _ReferencePosition, Vector* _ScrollDirection);

/*
* Check if we can scroll in specified direction
*/
UINT8 ScrollManager_CanScroll_Free(Vector* _ReferencePosition, Vector* _ScrollDirection);

/** Static Variables **/

// Current scroll value
static Vector s_ScrollOffset = {0,0};

// Number of tile scrolled (for scroll mode X or Y ONLY)
static UINT8 s_NbTileScrolled = 0;

// Desired scroll offset
static Vector s_DesiredScrollOffset = {0,0};

// Current scroll mode
static EScrollMode s_CurrentScrollMode = SCROLLMODE_FREE;

#endif