#ifndef DEF_BOX

#define DEF_BOX

#include <gb/gb.h>

#include "Vector.h"

/** Structures **/

// Struct used to define all data for a box
struct Box
{
    // Position of top left corner on x axis
    UINT8 x;

    // Position of top left corner on y axis
    UINT8 y;

    // Box width
    UINT8 width;

    // Box height
    UINT8 height;
};
typedef struct Box Box;

/** Exposed function **/

/*
* Check if 2 box intersect
*/
UINT8 Box_Intersect(Box* _BoxA, Box* _BoxB);

/*
* Get position of top left corner as a Vector
*/
void Box_GetPosition(Box* _Box, Vector* _OutPosition);

/*
* Set position of top left corner using a Vector
*/
void Box_SetPosition(Box* _Box, Vector* _Position);

/*
* Copy all values of source box to destination box
*/
void Box_Copy(Box* _SourceBox, Box* _DestinationBox);

#endif