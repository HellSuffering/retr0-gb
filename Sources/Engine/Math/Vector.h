#ifndef DEF_VECTOR

#define DEF_VECTOR

#include <gb/gb.h>

/** Structs **/

// Struct used to define all data for a vector
struct Vector
{
    // Vector X component
    INT8 x;

    // Vector Y component
    INT8 y;
};
typedef struct Vector Vector;

/** Exposed Functions **/

/*
* Get copy of zero vector
*/
void Vector_GetVectorZero(Vector* _OutVector);

/*
* Clamp vector size to 1 (vector must be different of zero vector)
*/
void Vector_Normalize(Vector* _Vector);

/*
* Add u.x to v.x and u.y to v.y. Return the result vector as a new vector
*/
void Vector_Add(Vector* _U, Vector* _V, Vector* _OutVector);

/*
* Subtract v.x to u.x and v.y to u.y. Return the result vector as a new vector
*/
void Vector_Subtract(Vector* _U, Vector* _V, Vector* _OutVector);

/*
* Multiply a vector with a UINT8
*/
void Vector_Multiply(Vector* _Vector, INT8 _Number);

/*
* Divide a vector by a UINT8
*/
void Vector_Divide(Vector* _Vector, INT8 _Number);

/*
* Copy components of source vector to destination vector
*/
void Vector_CopyTo(Vector* _SourceVector, Vector* _DestinationVector);

/*
* Check for components equality (u.x == v.x && u.y == v.y)
*/
UINT8 Vector_Equals(Vector* _U, Vector* _V);

/** Static Variables **/

// Zero vector
static Vector s_VectorZero = { 0, 0 };

#endif