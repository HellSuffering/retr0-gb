#include "Vector.h"

/*
* Clamp vector size to 1 (vector must be different of zero vector)
*/
void Vector_Normalize(Vector* _Vector)
{
    if (_Vector != NULL)
    {
        if (_Vector->x != 0)
            _Vector->x = _Vector->x > 0 ? 1 : -1;
        
        if (_Vector->y != 0)
            _Vector->y = _Vector->y > 0 ? 1 : -1;
    }
}

/*
* Get copy of zero vector
*/
void Vector_GetVectorZero(Vector* _OutVector)
{
    if (_OutVector != NULL)
    {
        _OutVector->x = _OutVector->x;
        _OutVector->y = _OutVector->y;
    }
}

/*
* Add u.x to v.x and u.y to v.y. Return the result vector as a new vector
*/
void Vector_Add(Vector* _U, Vector* _V, Vector* _OutVector)
{
    if (_OutVector != NULL && _U != NULL && _V != NULL)
    {
        _OutVector->x =_U->x + _V->x;
        _OutVector->y = _U->y + _V->y;
    }
}

/*
* Subtract v.x to u.x and v.y to u.y. Return the result vector as a new vector
*/
void Vector_Subtract(Vector* _U, Vector* _V, Vector* _OutVector)
{
    if (_U != NULL && _V != NULL && _OutVector != NULL)
    {
        _OutVector->x = _U->x - _V->x;
        _OutVector->y = _U->y - _V->y;
    }
}

/*
* Multiply a vector with a UINT8
*/
void Vector_Multiply(Vector* _Vector, INT8 _Number)
{
    if (_Vector != NULL)
    {
        _Vector->x *= _Number;
        _Vector->y *= _Number;
    }
}

/*
* Divide a vector by a UINT8
*/
void Vector_Divide(Vector* _Vector, INT8 _Number)
{
    if (_Vector != NULL)
    {
        _Vector->x /= _Number;
        _Vector->y /= _Number;
    }
}

/*
* Copy components of source vector to destination vector
*/
void Vector_CopyTo(Vector* _SourceVector, Vector* _DestinationVector)
{
    if (_SourceVector != NULL && _DestinationVector != NULL)
    {
        _DestinationVector->x = _SourceVector->x;
        _DestinationVector->y = _SourceVector->y;
    }
}

/*
* Check for components equality (u.x == v.x && u.y == v.y)
*/
UINT8 Vector_Equals(Vector* _U, Vector* _V)
{
    return (_U != NULL && _V != NULL) ? (_U->x == _V->x && _U->y == _V->y) : FALSE;
}