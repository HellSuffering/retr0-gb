#include "Box.h"

/*
* Check if 2 box intersect
*/
UINT8 Box_Intersect(Box* _BoxA, Box* _BoxB)
{
    // Check boxes are not intersecting
    if (_BoxB->x >= _BoxA->x + _BoxA->width || _BoxB->x + _BoxB->width <= _BoxA->x 
        || _BoxB->y >= _BoxA->y + _BoxA->height || _BoxB->y + _BoxB->height <= _BoxA->y)
        return FALSE;

    return TRUE;
}

/*
* Get position of top left corner as a Vector
*/
void Box_GetPosition(Box* _Box, Vector* _OutPosition)
{
    if (_OutPosition != NULL)
    {
        Vector_GetVectorZero(_OutPosition);

        if (_Box != NULL)
        {
            _OutPosition->x = _Box->x;
            _OutPosition->y = _Box->y;
        }
    }
}

/*
* Set position of top left corner using a Vector
*/
void Box_SetPosition(Box* _Box, Vector* _Position)
{
    if (_Box != NULL && _Position != NULL)
    {
        _Box->x = _Position->x;
        _Box->y = _Position->y;
    }
}

/*
* Copy all values of source box to destination box
*/
void Box_Copy(Box* _SourceBox, Box* _DestinationBox)
{
    if (_SourceBox != NULL && _DestinationBox != NULL)
    {
        _DestinationBox->x = _SourceBox->x;
        _DestinationBox->y = _SourceBox->y;
        _DestinationBox->width = _SourceBox->width;
        _DestinationBox->height = _SourceBox->height;
    }
}