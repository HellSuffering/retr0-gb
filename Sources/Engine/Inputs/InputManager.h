#ifndef DEF_INPUTMANAGER

#define DEF_INPUTMANAGER

#include <gb/gb.h>

/** Exposed Functions **/

/*
* Called each frame at game loop
*/
void InputManager_Update();

/*
* Check if button of specified ID is down
*/
UINT8 InputManager_IsButtonDown(UINT8 _ButtonID);

/*
* Check if button of specified ID has been pressed this frame
*/
UINT8 InputManager_IsButtonPressed(UINT8 _ButtonID);

/*
* Check if button of specified ID has been released this frame
*/
UINT8 InputManager_IsButtonReleased(UINT8 _ButtonID);

/*
* Set input manager enabled / disabled. Disable input manager to force return no input
*/
UINT8 InputManager_SetInputEnabled(UINT8 _Enabled);

/** Static Variables **/

// All buttons down at this frame
static UINT8 s_CurrentButtonsDown = 0;

// All buttons down at previous frame
static UINT8 s_PreviousButtonsDown = 0;

// Is input manager enabled or not
static UINT8 s_InputEnabled = TRUE;

#endif