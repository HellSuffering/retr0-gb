#include "InputManager.h"

/*
* Called each frame at game loop
*/
void InputManager_Update()
{
    if (s_InputEnabled)
    {
        s_PreviousButtonsDown = s_CurrentButtonsDown;
        s_CurrentButtonsDown = joypad();
    }
    else
    {
        s_PreviousButtonsDown = 0;
        s_CurrentButtonsDown = 0;
    }
}

/*
* Check if button of specified ID is down
*/
UINT8 InputManager_IsButtonDown(UINT8 _ButtonID)
{
    return s_CurrentButtonsDown & _ButtonID;
}

/*
* Check if button of specified ID has been pressed this frame
*/
UINT8 InputManager_IsButtonPressed(UINT8 _ButtonID)
{
    return (s_CurrentButtonsDown & _ButtonID) && !(s_PreviousButtonsDown & _ButtonID);
}

/*
* Check if button of specified ID has been released this frame
*/
UINT8 InputManager_IsButtonReleased(UINT8 _ButtonID)
{
    return (s_PreviousButtonsDown & _ButtonID) && !(s_CurrentButtonsDown & _ButtonID);
}

/*
* Set input manager enabled / disabled. Disable input manager to force return no input
*/
UINT8 InputManager_SetInputEnabled(UINT8 _Enabled)
{
    s_InputEnabled = _Enabled;
}