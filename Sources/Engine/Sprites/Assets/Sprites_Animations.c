#include "Sprites_Animations.h"
#include "Sprites_TileID.h"

/** Animations **/

// Ghost animation 8x8
SpriteAnim SpriteAnim_Ghost8x8;

// Ghost animation 16x16
SpriteAnim SpriteAnim_Ghost16x16_Left;
SpriteAnim SpriteAnim_Ghost16x16_Right;

// Player animations
SpriteAnim SpriteAnim_PlayerMoveDown_Left;
SpriteAnim SpriteAnim_PlayerMoveDown_Right;
SpriteAnim SpriteAnim_PlayerMoveRight_Left;
SpriteAnim SpriteAnim_PlayerMoveRight_Right;
SpriteAnim SpriteAnim_PlayerMoveUp_Left;
SpriteAnim SpriteAnim_PlayerMoveUp_Right;
SpriteAnim SpriteAnim_PlayerMoveLeft_Left;
SpriteAnim SpriteAnim_PlayerMoveLeft_Right;

/** Functions **/

/*
* Initialize all sprite anim variables
*/
void SpritesAnimations_Initialization()
{
    // Initialize sprite anim ghost 8x8
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_Ghost8x8, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost8x8, 0, SPRITES_TILESET0_GHOST0, 30);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost8x8, 1, SPRITES_TILESET0_GHOST1, 30);

    // Initialize sprite anim ghost 16x16
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_Ghost16x16_Left, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost16x16_Left, 0, SPRITES_TILESET16x16_GHOST_LEFT_0, 30);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost16x16_Left, 1, SPRITES_TILESET16x16_GHOST_LEFT_1, 30);
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_Ghost16x16_Right, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost16x16_Right, 0, SPRITES_TILESET16x16_GHOST_RIGHT_0, 30);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_Ghost16x16_Right, 1, SPRITES_TILESET16x16_GHOST_RIGHT_1, 30);

    // Anim player move down
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveDown_Left, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveDown_Left, 0, SPRITES_TILESET16x16_PLAYER_DOWN_WALK0_LEFT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveDown_Left, 1, SPRITES_TILESET16x16_PLAYER_DOWN_WALK1_LEFT, 15);
    
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveDown_Right, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveDown_Right, 0, SPRITES_TILESET16x16_PLAYER_DOWN_WALK0_RIGHT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveDown_Right, 1, SPRITES_TILESET16x16_PLAYER_DOWN_WALK1_RIGHT, 15);

    // Anim player move right
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveRight_Left, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveRight_Left, 0, SPRITES_TILESET16x16_PLAYER_RIGHT_WALK0_LEFT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveRight_Left, 1, SPRITES_TILESET16x16_PLAYER_RIGHT_WALK1_LEFT, 15);
    
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveRight_Right, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveRight_Right, 0, SPRITES_TILESET16x16_PLAYER_RIGHT_WALK0_RIGHT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveRight_Right, 1, SPRITES_TILESET16x16_PLAYER_RIGHT_WALK1_RIGHT, 15);

    // Anim player move left
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveLeft_Left, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveLeft_Left, 0, SPRITES_TILESET16x16_PLAYER_LEFT_WALK0_LEFT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveLeft_Left, 1, SPRITES_TILESET16x16_PLAYER_LEFT_WALK1_LEFT, 15);
    
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveLeft_Right, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveLeft_Right, 0, SPRITES_TILESET16x16_PLAYER_LEFT_WALK0_RIGHT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveLeft_Right, 1, SPRITES_TILESET16x16_PLAYER_LEFT_WALK1_RIGHT, 15);

    // Anim player move up
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveUp_Left, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveUp_Left, 0, SPRITES_TILESET16x16_PLAYER_UP_WALK0_LEFT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveUp_Left, 1, SPRITES_TILESET16x16_PLAYER_UP_WALK1_LEFT, 15);
    
    SpriteAnim_IntializeSpriteAnim(&SpriteAnim_PlayerMoveUp_Right, 2);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveUp_Right, 0, SPRITES_TILESET16x16_PLAYER_UP_WALK0_RIGHT, 15);
    SpriteAnim_SetSpriteAnimFrame(&SpriteAnim_PlayerMoveUp_Right, 1, SPRITES_TILESET16x16_PLAYER_UP_WALK1_RIGHT, 15);
}