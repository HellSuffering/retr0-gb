#include "Sprites_Tilesets.h"

// Sprites tileset 0
const unsigned char Sprites_Tileset0[] = 
{
  0x7E,0x7E,0xFF,0x81,0xFF,0xA5,0xFF,0x81,
  0xFF,0xBD,0xE7,0xBD,0xFF,0x99,0x7E,0x7E,
  0x7E,0x7E,0xFF,0x81,0xFF,0xA5,0xFF,0x81,
  0xFF,0x81,0xFF,0xBD,0xFF,0x81,0x7E,0x7E,
  0x7E,0x7E,0x8F,0xF1,0x9F,0xE5,0x9F,0xE1,
  0xBF,0xC7,0xFF,0x85,0xFF,0x83,0x7E,0x7E,
  0x7E,0x7E,0x8F,0xF1,0x9F,0xE5,0x9F,0xE1,
  0xBF,0xC1,0xFF,0x87,0xFF,0x81,0x7E,0x7E,
  0x7E,0x7E,0xF1,0x8F,0xF9,0xA7,0xF9,0x87,
  0xFD,0xE3,0xFF,0xA1,0xFF,0xC1,0x7E,0x7E,
  0x7E,0x7E,0xF1,0x8F,0xF9,0xA7,0xF9,0x87,
  0xFD,0x83,0xFF,0xE1,0xFF,0x81,0x7E,0x7E,
  0x7E,0x7E,0x81,0xFF,0x81,0xFF,0x81,0xFF,
  0x81,0xFF,0xC3,0xBD,0xFF,0x81,0x7E,0x7E,
  0x7E,0x7E,0x81,0xFF,0x81,0xFF,0x81,0xFF,
  0x81,0xFF,0xC3,0xBD,0xE7,0x99,0x7E,0x7E,
  0x3C,0x3C,0x46,0x7A,0x83,0xFD,0x83,0xFD,
  0x83,0xFD,0x83,0xFD,0x42,0x7E,0x3C,0x3C,
  0x66,0x66,0x9F,0xF9,0x83,0xFD,0x81,0xFF,
  0x81,0xFF,0x42,0x7E,0x24,0x3C,0x18,0x18,
  0x24,0x24,0x5A,0x7E,0xFF,0x81,0xFF,0xA5,
  0xFF,0x81,0xFF,0xBD,0xE7,0xBD,0x7E,0x7E,
  0x3C,0x3C,0x7E,0x42,0xFF,0xA5,0xFF,0x81,
  0xFF,0x99,0xFF,0x81,0xFF,0xA5,0x5A,0x5A,
  0x3C,0x3C,0x7E,0x42,0xFF,0xA5,0xFF,0x81,
  0xFF,0x81,0xFF,0xA5,0x5A,0x5A,0x00,0x00
};

// Sprites tileset 16x16
const unsigned char Sprites_Tileset16x16[] =
{
    0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x3B,0x3C,0x3F,0x37,0x7F,0x50,0x7F,0x42,
  0x3F,0x32,0x3E,0x39,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x11,0x1F,0x0E,0x0E,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0xDC,0x3C,0xFC,0xEC,0xFE,0x0A,0xFE,0x42,
  0xFE,0x4C,0x7C,0x9C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0x88,0xF8,0x70,0x70,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x13,0x1C,
  0x37,0x38,0x37,0x3A,0x7F,0x52,0x7F,0x40,
  0x3F,0x33,0x3F,0x3A,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x11,0x1F,0x0E,0x0E,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0xC8,0x38,
  0xEC,0x1C,0xEC,0x5C,0xFE,0x4A,0xFE,0x02,
  0xFC,0xCC,0xFC,0x5C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0x88,0xF8,0x70,0x70,
  0x0F,0x0F,0x1F,0x10,0x3F,0x26,0x79,0x4F,
  0x7B,0x4F,0x7B,0x4F,0x7F,0x46,0x7F,0x40,
  0x7F,0x41,0x7E,0x43,0x7F,0x41,0x7F,0x40,
  0x7F,0x40,0x7F,0x56,0x29,0x29,0x00,0x00,
  0xF0,0xF0,0xF8,0x08,0xFC,0x64,0x9E,0xF2,
  0xDE,0xF2,0xDE,0xF2,0xFE,0x62,0xFE,0x02,
  0xFE,0x82,0x7E,0xC2,0xFE,0x82,0xFE,0x02,
  0xFE,0x02,0xFE,0x6A,0x94,0x94,0x00,0x00,
  0x0F,0x0F,0x1F,0x10,0x3F,0x26,0x79,0x4F,
  0x7B,0x4F,0x7B,0x4F,0x7F,0x46,0x7F,0x40,
  0x7F,0x41,0x7E,0x43,0x7F,0x41,0x7F,0x40,
  0x7F,0x56,0x29,0x29,0x00,0x00,0x00,0x00,
  0xF0,0xF0,0xF8,0x08,0xFC,0x64,0x9E,0xF2,
  0xDE,0xF2,0xDE,0xF2,0xFE,0x62,0xFE,0x02,
  0xFE,0x82,0x7E,0xC2,0xFE,0x82,0xFE,0x02,
  0xEE,0x6A,0x94,0x94,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x4E,0x72,0x83,0xFD,0x83,0xFD,
  0x83,0xFD,0x83,0xFD,0x42,0x7E,0x3C,0x3C,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x66,0x66,0x9F,0xF9,0x83,0xFD,0x81,0xFF,
  0x81,0xFF,0x42,0x7E,0x24,0x3C,0x18,0x18,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x38,0x3F,0x37,0x3F,0x70,0x5F,0x70,0x4F,
  0x38,0x37,0x3F,0x38,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x11,0x1F,0x0E,0x0E,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0x1C,0xFC,0xEC,0xFC,0x0E,0xFA,0x0E,0xF2,
  0x1E,0xEC,0xFC,0x1C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0x88,0xF8,0x70,0x70,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x38,0x3F,0x37,0x3F,0x70,0x5F,0x70,0x4F,
  0x38,0x37,0x3F,0x38,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x11,0x1F,0x0E,0x0E,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0x1C,0xFC,0xEC,0xFC,0x0E,0xFA,0x0E,0xF2,
  0x1E,0xEC,0xFC,0x1C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0xF0,0xF0,0x00,0x00,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x38,0x3F,0x37,0x3F,0x70,0x5F,0x70,0x4F,
  0x38,0x37,0x3F,0x38,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x0F,0x0F,0x00,0x00,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0x1C,0xFC,0xEC,0xFC,0x0E,0xFA,0x0E,0xF2,
  0x1E,0xEC,0xFC,0x1C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0x88,0xF8,0x70,0x70,
  0x1F,0x1F,0x20,0x3F,0x40,0x7F,0x40,0x7F,
  0x60,0x7F,0xFF,0xFE,0xFF,0xFC,0xFF,0xCC,
  0x7F,0x44,0xBF,0xF0,0x8F,0xFF,0x8F,0xF9,
  0x9F,0xF9,0x7E,0x7F,0x08,0x0F,0x07,0x07,
  0xE0,0xE0,0x30,0xF0,0x08,0xF8,0x06,0xFE,
  0x07,0xF9,0xFE,0x0E,0xF8,0x28,0xF8,0x28,
  0xF8,0x08,0xF0,0x10,0xE0,0xE0,0x80,0x80,
  0xC0,0xC0,0x20,0xE0,0x20,0xE0,0xC0,0xC0,
  0x1F,0x1F,0x20,0x3F,0x40,0x7F,0x40,0x7F,
  0x60,0x7F,0xFF,0xFE,0xFF,0xFC,0xFF,0xCC,
  0x7F,0x44,0xBF,0xF0,0x8F,0xFF,0x8F,0xF9,
  0x9F,0xF9,0x7F,0x7F,0x11,0x1F,0x0F,0x0F,
  0xE0,0xE0,0x30,0xF0,0x08,0xF8,0x06,0xFE,
  0x07,0xF9,0xFE,0x0E,0xF8,0x28,0xF8,0x28,
  0xF8,0x08,0xF0,0x10,0xE0,0xE0,0x80,0x80,
  0xF0,0xF0,0x88,0xF8,0x08,0xF8,0xF0,0xF0,
  0x07,0x07,0x0C,0x0F,0x10,0x1F,0x60,0x7F,
  0xE0,0x9F,0x7F,0x70,0x1F,0x14,0x1F,0x14,
  0x1F,0x10,0x0F,0x08,0x07,0x07,0x01,0x01,
  0x03,0x03,0x04,0x07,0x04,0x07,0x03,0x03,
  0xF8,0xF8,0x04,0xFC,0x02,0xFE,0x02,0xFE,
  0x06,0xFE,0xFF,0x7F,0xFF,0x3F,0xFF,0x33,
  0xFE,0x22,0xFD,0x0F,0xF1,0xFF,0xF1,0x9F,
  0xF9,0x9F,0x7E,0xFE,0x10,0xF0,0xE0,0xE0,
  0x07,0x07,0x0C,0x0F,0x10,0x1F,0x60,0x7F,
  0xE0,0x9F,0x7F,0x70,0x1F,0x14,0x1F,0x14,
  0x1F,0x10,0x0F,0x08,0x07,0x07,0x01,0x01,
  0x0F,0x0F,0x11,0x1F,0x10,0x1F,0x0F,0x0F,
  0xF8,0xF8,0x04,0xFC,0x02,0xFE,0x02,0xFE,
  0x06,0xFE,0xFF,0x7F,0xFF,0x3F,0xFF,0x33,
  0xFE,0x22,0xFD,0x0F,0xF1,0xFF,0xF1,0x9F,
  0xF9,0x9F,0xFE,0xFE,0x88,0xF8,0xF0,0xF0,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x3B,0x3C,0x3F,0x37,0x7F,0x50,0x7F,0x42,
  0x3F,0x32,0x3E,0x39,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x0F,0x0F,0x00,0x00,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0xDC,0x3C,0xFC,0xEC,0xFE,0x0A,0xFE,0x42,
  0xFE,0x4C,0x7C,0x9C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0x88,0xF8,0x70,0x70,
  0x07,0x07,0x08,0x0F,0x10,0x1F,0x10,0x1F,
  0x3B,0x3C,0x3F,0x37,0x7F,0x50,0x7F,0x42,
  0x3F,0x32,0x3E,0x39,0x7F,0x4F,0x7F,0x4F,
  0x39,0x3F,0x16,0x1F,0x11,0x1F,0x0E,0x0E,
  0xE0,0xE0,0x10,0xF0,0x08,0xF8,0x08,0xF8,
  0xDC,0x3C,0xFC,0xEC,0xFE,0x0A,0xFE,0x42,
  0xFE,0x4C,0x7C,0x9C,0xFE,0xF2,0xFE,0xF2,
  0x9C,0xFC,0x68,0xF8,0xF0,0xF0,0x00,0x00
};