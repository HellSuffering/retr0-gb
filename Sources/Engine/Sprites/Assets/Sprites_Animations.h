#ifndef DEF_SPRITES_ANIMATIONS

#define DEF_SPRITES_ANIMATIONS

#include "../SpriteAnim.h"

/** Exposed Functions **/

/*
* Initialize all sprite anim variables
*/
void SpritesAnimations_Initialization();

/** Animations **/

// Ghost animation 8x8
extern SpriteAnim SpriteAnim_Ghost8x8;

// Ghost animation 16x16
extern SpriteAnim SpriteAnim_Ghost16x16_Left;
extern SpriteAnim SpriteAnim_Ghost16x16_Right;

// Player animations
extern SpriteAnim SpriteAnim_PlayerMoveDown_Left;
extern SpriteAnim SpriteAnim_PlayerMoveDown_Right;
extern SpriteAnim SpriteAnim_PlayerMoveRight_Left;
extern SpriteAnim SpriteAnim_PlayerMoveRight_Right;
extern SpriteAnim SpriteAnim_PlayerMoveUp_Left;
extern SpriteAnim SpriteAnim_PlayerMoveUp_Right;
extern SpriteAnim SpriteAnim_PlayerMoveLeft_Left;
extern SpriteAnim SpriteAnim_PlayerMoveLeft_Right;

#endif