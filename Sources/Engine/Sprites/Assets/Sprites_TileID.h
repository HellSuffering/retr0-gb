#ifndef DEF_SPRITES_TILEID

#define DEF_SPRITES_TILEID

// Tiles ID for sprites tileset 0
#define SPRITES_TILESET0_PLAYER_DOWN0 0
#define SPRITES_TILESET0_PLAYER_DOWN1 1
#define SPRITES_TILESET0_PLAYER_RIGHT0 2
#define SPRITES_TILESET0_PLAYER_RIGHT1 3
#define SPRITES_TILESET0_PLAYER_LEFT0 4
#define SPRITES_TILESET0_PLAYER_LEFT1 5
#define SPRITES_TILESET0_PLAYER_UP0 6
#define SPRITES_TILESET0_PLAYER_UP1 7
#define SPRITES_TILESET0_COIN 8
#define SPRITES_TILESET0_LIVE 9
#define SPRITES_TILESET0_ENEMY 10
#define SPRITES_TILESET0_GHOST0 11
#define SPRITES_TILESET0_GHOST1 12

// Tiles ID for sprites tileset 16x16
#define SPRITES_TILESET16x16_PLAYER_LEFT 0
#define SPRITES_TILESET16x16_PLAYER_RIGHT 2
#define SPRITES_TILESET16x16_ENEMY_LEFT 4
#define SPRITES_TILESET16x16_ENEMY_RIGHT 6
#define SPRITES_TILESET16x16_GHOST_LEFT_0 8
#define SPRITES_TILESET16x16_GHOST_RIGHT_0 10
#define SPRITES_TILESET16x16_GHOST_LEFT_1 12
#define SPRITES_TILESET16x16_GHOST_RIGHT_1 14
#define SPRITES_TILESET16x16_PICKUP_COIN 16
#define SPRITES_TILESET16x16_PICKUP_LIVE 18

#define SPRITES_TILESET16x16_PLAYER_UP_LEFT 20
#define SPRITES_TILESET16x16_PLAYER_UP_RIGHT 22
#define SPRITES_TILESET16x16_PLAYER_UP_WALK0_LEFT 24
#define SPRITES_TILESET16x16_PLAYER_UP_WALK0_RIGHT 26
#define SPRITES_TILESET16x16_PLAYER_UP_WALK1_LEFT 28
#define SPRITES_TILESET16x16_PLAYER_UP_WALK1_RIGHT 30
#define SPRITES_TILESET16x16_PLAYER_RIGHT_LEFT 32
#define SPRITES_TILESET16x16_PLAYER_RIGHT_RIGHT 34
#define SPRITES_TILESET16x16_PLAYER_RIGHT_WALK0_LEFT 32
#define SPRITES_TILESET16x16_PLAYER_RIGHT_WALK0_RIGHT 34
#define SPRITES_TILESET16x16_PLAYER_RIGHT_WALK1_LEFT 36
#define SPRITES_TILESET16x16_PLAYER_RIGHT_WALK1_RIGHT 38
#define SPRITES_TILESET16x16_PLAYER_LEFT_LEFT 40
#define SPRITES_TILESET16x16_PLAYER_LEFT_RIGHT 42
#define SPRITES_TILESET16x16_PLAYER_LEFT_WALK0_LEFT 40
#define SPRITES_TILESET16x16_PLAYER_LEFT_WALK0_RIGHT 42
#define SPRITES_TILESET16x16_PLAYER_LEFT_WALK1_LEFT 44
#define SPRITES_TILESET16x16_PLAYER_LEFT_WALK1_RIGHT 46
#define SPRITES_TILESET16x16_PLAYER_DOWN_LEFT 48
#define SPRITES_TILESET16x16_PLAYER_DOWN_RIGHT 50
#define SPRITES_TILESET16x16_PLAYER_DOWN_WALK0_LEFT 48
#define SPRITES_TILESET16x16_PLAYER_DOWN_WALK0_RIGHT 50
#define SPRITES_TILESET16x16_PLAYER_DOWN_WALK1_LEFT 52
#define SPRITES_TILESET16x16_PLAYER_DOWN_WALK1_RIGHT 54


#define SPRITES_NOTILE 254

#endif