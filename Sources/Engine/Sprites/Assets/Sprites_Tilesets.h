#ifndef DEF_SPRITES_TILESETS

#define DEF_SPRITES_TILESETS

#define SPRITES_TILESET0_NBTILE 13
#define SPRITES_TILESET16x16_NBTILE 56

// Sprites tileset 0
extern const unsigned char Sprites_Tileset0[];

// Sprites tileset 16x16
extern const unsigned char Sprites_Tileset16x16[];

#endif