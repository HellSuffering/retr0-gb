#ifndef DEF_SPRITEANIM

#define DEF_SPRITEANIM

#include <gb/gb.h>

#define SPRITEANIM_NBFRAMES 2

/** Structs **/

// Struct used to group all data for a sprite animation frame
struct SpriteAnimFrame
{
    // ID of tile to display
    UINT8 tileID;

    // Time to wait on this sprite anim frame in number of frame
    UINT8 duration;

    // Number of frame elapsed on current sprite anim frame
    UINT8 nbFrameElapsed;
};
typedef struct SpriteAnimFrame SpriteAnimFrame;

// Struct used to group all data for a sprite animation
struct SpriteAnim
{
    // Array of all sprite anim frame composing the sprite animation
    //SpriteAnimFrame* spriteAnimFrameArray;
    SpriteAnimFrame spriteAnimFrameArray[SPRITEANIM_NBFRAMES];

    // Length of sprite anim frame array
    UINT8 nbSpriteAnimFrame;

    // Index of current sprite anim frame in sprite anim frame array
    UINT8 currentSpriteAnimFrameIndex;
};
typedef struct SpriteAnim SpriteAnim;

/** Exposed Functions **/

/*
* Initialize a sprite anim creating an array of sprite anim frame of specified length
*/
void SpriteAnim_IntializeSpriteAnim(SpriteAnim* _SpriteAnim, UINT8 _NbSpriteAnimFrame);

/*
* Initialize sprite anim frame data at specified index with specified values
*/
void SpriteAnim_SetSpriteAnimFrame(SpriteAnim* _SpriteAnim, UINT8 _Index, UINT8 _TileID, UINT8 _Duration);

/*
* Update sprite anim. Check which sprite anim frame to make active return its tile ID. Return SPRITES_NOTILE if no anim
*/
UINT8 SpriteAnim_UpdateSpriteAnim(SpriteAnim* _SpriteAnim);

#endif