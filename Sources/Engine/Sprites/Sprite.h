#include <gb/gb.h>

#include "Assets/Sprites_TileID.h"
#include "Assets/Sprites_Animations.h"

#include "SpriteAnim.h"
#include "../Math/Vector.h"

#define SPRITE_NBMAX 40

/** Structures **/

// Struct used to group all sprite data
struct Sprite
{
    // ID of sprite in memory
    UINT8 ID;

    // ID of tile used for this sprite
    UINT8 tileID;

    // Sprite position on screen
    Vector screenPosition;

    // Desired sprite position. Use to move sprite to this position autmatically on several frames
    Vector desiredPosition;

    // Sprite anim used for this sprite
    SpriteAnim* spriteAnim;

    // Offset to apply on position X. Used if this sprite is the right sprite of a movable
    UINT8 offsetX;
};
typedef struct Sprite Sprite;

/** Exposed Functions **/

/*
* Load sprite tileset in memory
*/
void Sprite_LoadSpriteTileset(UINT8* _Tiles, UINT8 _NbTiles);

/*
* Set all sprite struct data and load it in gb memory
*/
void Sprite_SetSpriteData(Sprite* _Sprite, UINT8 _SpriteID, UINT8 _TileID, UINT8 _IsRightSprite);

/*
* Move sprite to a new position defined by a Vector
*/
void Sprite_MoveSprite(Sprite* _Sprite, Vector* _Position);

/*
* Translate sprite of 1 "pixel" in desired position direction
*/
void Sprite_MoveToDesiredPosition(Sprite* _Sprite);

/*
* Set sprite desired position from a movable position (adjust position with sprite offset)
*/
void Sprite_SetDesiredPosition(Sprite* _Sprite, Vector* _DesiredPosition);

/*
* Check if sprite screen position equals sprite desired position
*/
UINT8 Sprite_IsAtDesiredPosition(Sprite* _Sprite);

/*
* Get first free sprite ID found in sprite ID array. Return SPRITE_NOTILE if no free sprite ID found
*/
UINT8 Sprite_GetFirstFreeSpriteID();

/*
* Set sprite ID as free setting value at ID index in sprite ID array to TRUE
*/
void Sprite_FreeID(UINT8 _ID);

/*
* Set sprite ID as used setting value at ID index in sprite ID array to FALSE
*/
void Sprite_UseID(UINT8 _ID);

/*
* Perform update operations as updating sprite animation
*/
void Sprite_UpdateSprite(Sprite* _Sprite);

/** Static Attributes **/

// Array of all sprite ID. Index is sprite ID and value is bool value for FREE/USED (FREE == TRUE, USED == FALSE)
static UINT8 s_SpriteIDArray[SPRITE_NBMAX] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};