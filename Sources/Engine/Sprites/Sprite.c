#include "Sprite.h"

#include "../Helpers/SpaceHelper.h"

/*
* Load sprite tileset in memory
*/
void Sprite_LoadSpriteTileset(UINT8* _Tiles, UINT8 _NbTiles)
{
    // Load the sprites tiles into sprite memory
    set_sprite_data(0, _NbTiles, _Tiles);
}

/*
* Set all sprite struct data and load it in gb memory
*/
void Sprite_SetSpriteData(Sprite* _Sprite, UINT8 _SpriteID, UINT8 _TileID, UINT8 _IsRightSprite)
{
    if (_Sprite != NULL && _Sprite->ID != SPRITES_NOTILE)
    {
        _Sprite->ID = _SpriteID;
        _Sprite->tileID = _TileID;
        _Sprite->offsetX = _IsRightSprite ? 8 : 0;

        // Set tile to use for the sprite
        if (_SpriteID != SPRITES_NOTILE)
        {
            set_sprite_tile(_SpriteID, _TileID);
            Sprite_UseID(_SpriteID);
        }
    }
}

/*
* Move sprite to a new position defined by a Vector
*/
void Sprite_MoveSprite(Sprite* _Sprite, Vector* _Position)
{
    if (_Sprite != NULL && _Position != NULL && _Sprite->ID != SPRITES_NOTILE)
    {
        Vector_CopyTo(_Position, &_Sprite->screenPosition);
        _Sprite->screenPosition.x += 8;
        _Sprite->screenPosition.y += 16;
        move_sprite(_Sprite->ID, _Sprite->screenPosition.x + _Sprite->offsetX, _Sprite->screenPosition.y);
    }
}

/*
* Translate sprite of 1 "pixel" in desired position direction
*/
void Sprite_MoveToDesiredPosition(Sprite* _Sprite)
{
    // Local variable
    Vector newPosition, translation = {0,0};

    if (_Sprite != NULL)
    {
        // Get new position
        Vector_Subtract(&_Sprite->desiredPosition, &_Sprite->screenPosition, &translation);
        Vector_Normalize(&translation);
        newPosition.x = _Sprite->screenPosition.x + translation.x;
        newPosition.y = _Sprite->screenPosition.y + translation.y;

        // Move sprite
        move_sprite(_Sprite->ID, newPosition.x + _Sprite->offsetX, newPosition.y);

        // Update screen position variable
        Vector_CopyTo(&newPosition, &_Sprite->screenPosition);
    }
}

/*
* Set sprite desired position from a movable position (adjust position with sprite offset)
*/
void Sprite_SetDesiredPosition(Sprite* _Sprite, Vector* _DesiredPosition)
{
    if (_Sprite != NULL && _DesiredPosition != NULL && _Sprite->ID != SPRITES_NOTILE)
    {
        _Sprite->desiredPosition.x = _DesiredPosition->x + 8;
        _Sprite->desiredPosition.y = _DesiredPosition->y + 16;
    }
}

/*
* Check if sprite screen position equals sprite desired position
*/
UINT8 Sprite_IsAtDesiredPosition(Sprite* _Sprite)
{
    if (_Sprite != NULL && _Sprite->ID != SPRITES_NOTILE)
        return Vector_Equals(&_Sprite->desiredPosition, &_Sprite->screenPosition);

    return FALSE;
}

/*
* Get first free sprite ID found in sprite ID array. Return SPRITE_NOTILE if no free sprite ID found
*/
UINT8 Sprite_GetFirstFreeSpriteID()
{
    // Local variables
    UINT8 i;

    // Search for free sprite ID
    for (i = 0; i < SPRITE_NBMAX; i++)
    {
        if (s_SpriteIDArray[i] == TRUE)
            return i;
    }

    return SPRITES_NOTILE;
}

/*
* Set sprite ID as free setting value at ID index in sprite ID array to TRUE
*/
void Sprite_FreeID(UINT8 _ID)
{
    s_SpriteIDArray[_ID] = TRUE;
}

/*
* Set sprite ID as used setting value at ID index in sprite ID array to FALSE
*/
void Sprite_UseID(UINT8 _ID)
{
    s_SpriteIDArray[_ID] = FALSE;
}

/*
* Perform update operations as updating sprite animation
*/
void Sprite_UpdateSprite(Sprite* _Sprite)
{
    // Local variables
    UINT8 tileID;
    UINT8 isScreenPositionOnScreen = 2, isDesiredPositionOnScreen = 2;

    if (_Sprite != NULL && _Sprite->ID != SPRITES_NOTILE)
    {
        // Move to desired position
        if (!Vector_Equals(&_Sprite->screenPosition, &_Sprite->desiredPosition))
        {        
            isScreenPositionOnScreen = SpaceHelper_IsPositionInScreen(&_Sprite->screenPosition);
                    
            if (isScreenPositionOnScreen)
            {
                Sprite_MoveToDesiredPosition(_Sprite);
            }
            else
            {
                isDesiredPositionOnScreen = SpaceHelper_IsPositionInScreen(&_Sprite->desiredPosition);

                if (isDesiredPositionOnScreen)
                {
                    Vector_CopyTo(&_Sprite->desiredPosition, &_Sprite->screenPosition);
                    move_sprite(_Sprite->ID, _Sprite->desiredPosition.x, _Sprite->desiredPosition.y);
                }
            }
        }

        // Update sprite animation
        if (_Sprite->spriteAnim != NULL)
        {
            if (isScreenPositionOnScreen == 2)
                isScreenPositionOnScreen = SpaceHelper_IsPositionInScreen(&_Sprite->screenPosition);
                
            if (isScreenPositionOnScreen)
            {
                tileID = SpriteAnim_UpdateSpriteAnim(_Sprite->spriteAnim);

                if (tileID != SPRITES_NOTILE && tileID != _Sprite->tileID)
                {
                    _Sprite->tileID = tileID;
                    set_sprite_tile(_Sprite->ID, _Sprite->tileID);
                }
            }
        }
    }
}