#include "SpriteAnim.h"
#include "Sprite.h"

//#include <gb/malloc.h>

/*
* Initialize a sprite anim creating an array of sprite anim frame of specified length
*/
void SpriteAnim_IntializeSpriteAnim(SpriteAnim* _SpriteAnim, UINT8 _NbSpriteAnimFrame)
{
    if (_SpriteAnim != NULL)
    {
        //if (_NbSpriteAnimFrame > 0)
        //{
            //_SpriteAnim->spriteAnimFrameArray = malloc(_NbSpriteAnimFrame * sizeof(_NbSpriteAnimFrame));
            _SpriteAnim->nbSpriteAnimFrame = _NbSpriteAnimFrame;
            _SpriteAnim->currentSpriteAnimFrameIndex = 0;
        //}
        //else
        //{
            //_SpriteAnim->spriteAnimFrameArray = NULL;
        //}
    }
}

/*
* Initialize sprite anim frame data at specified index with specified values
*/
void SpriteAnim_SetSpriteAnimFrame(SpriteAnim* _SpriteAnim, UINT8 _Index, UINT8 _TileID, UINT8 _Duration)
{
    // Local variables
    SpriteAnimFrame* animFrame = NULL;

    // Initialize sprite anim frame
    if (_SpriteAnim != NULL && _Index < _SpriteAnim->nbSpriteAnimFrame)
    {
        animFrame = &_SpriteAnim->spriteAnimFrameArray[_Index];

        if (animFrame != NULL)
        {
            animFrame->tileID = _TileID;
            animFrame->duration = _Duration;
            animFrame->nbFrameElapsed = 0;
        }
    }
}

/*
* Update sprite anim. Check which sprite anim frame to make active return its tile ID. Return SPRITES_NOTILE if no anim
*/
UINT8 SpriteAnim_UpdateSpriteAnim(SpriteAnim* _SpriteAnim)
{
    // Local variables
    SpriteAnimFrame* animFrame = NULL;
    
    if (_SpriteAnim != NULL && _SpriteAnim->nbSpriteAnimFrame > 0)
    {
        animFrame = &_SpriteAnim->spriteAnimFrameArray[_SpriteAnim->currentSpriteAnimFrameIndex];

        if (animFrame != NULL)
        {
            // Update anim frame nb frame elapsed and check if we must take next anim frame
            if (animFrame->nbFrameElapsed < animFrame->duration)
            {
                animFrame->nbFrameElapsed++;
            }
            else
            {
                animFrame->nbFrameElapsed = 0;

                if (++_SpriteAnim->currentSpriteAnimFrameIndex >= _SpriteAnim->nbSpriteAnimFrame)
                    _SpriteAnim->currentSpriteAnimFrameIndex = 0;

                animFrame = &_SpriteAnim->spriteAnimFrameArray[_SpriteAnim->currentSpriteAnimFrameIndex];

                if (animFrame == NULL)
                    return SPRITES_NOTILE;
            }
            
            return animFrame->tileID;
        }
    }

    return SPRITES_NOTILE;
}