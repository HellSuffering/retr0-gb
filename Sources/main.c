#include <gb/gb.h>

#include "Engine/Game/Game.h"

#include "Engine/Inputs/InputManager.h"
#include "Engine/Movables/Movable.h"
#include "Engine/Maps/MapLoader.h"
#include "Engine/Tags_Layers/Tags.h"

#include "Engine/Dialogs/DialogManager.h"
#include "Engine/Audio/MusicPlayer.h"

#include "Engine/Scroll/ScrollManager.h"

void CheckInput();
void EnableMovement();
void OnDialogClosed(Dialog* _Dialog);

UINT8 playerCanMove = TRUE;

/*
* Main function of program. Entry point
*/
void main()
{
    // Play music
    disable_interrupts();
    MusicPlayer_Play(Song0, 7, TRUE);

    // Initialize game
    Game_Initialize();

    // Bind update callbacks
    Game_BindPostUpdateCallback(CheckInput);

    //MapLoader_RequestLoadMapData(MapTestScrollX_LOAD_PARAMS, NULL);
    //ScrollManager_SetScrollMode(SCROLLMODE_PLATFORMER);
}

/*
* React to inputs
*/
void CheckInput()
{
    // Local variables
    Vector translation = {0, 0};
    Movable* playerMovable = Movable_FindMovable(TAG_PLAYER);
    Dialog testDialog;

    // Load map
    if (InputManager_IsButtonPressed(J_B))
    {
        // Test dialog
        testDialog.activeSentence = &DialogSentence_MagicSword0;

        if (!DialogManager_DialogExists())
        {
            playerCanMove = FALSE;
            DialogManager_Open(&testDialog, OnDialogClosed);
        }
        else
        {
            DialogManager_Next();
        }
    }
    else if (InputManager_IsButtonPressed(J_A))
    {

    }

    // Move player
    if (playerMovable != NULL && playerCanMove)
    {
        if (InputManager_IsButtonDown(J_LEFT))
        {
            translation.x--;

            if (Movable_CanTryMoving(playerMovable))
                Movable_SetSpriteAnim(playerMovable, &SpriteAnim_PlayerMoveLeft_Left, &SpriteAnim_PlayerMoveLeft_Right);
        }
        else if (InputManager_IsButtonDown(J_RIGHT))
        {
            translation.x++;

            if (Movable_CanTryMoving(playerMovable))
                Movable_SetSpriteAnim(playerMovable, &SpriteAnim_PlayerMoveRight_Left, &SpriteAnim_PlayerMoveRight_Right);
        }
        else if (InputManager_IsButtonDown(J_UP))
        {
            translation.y--;

            if (Movable_CanTryMoving(playerMovable))
                Movable_SetSpriteAnim(playerMovable, &SpriteAnim_PlayerMoveUp_Left, &SpriteAnim_PlayerMoveUp_Right);
        }
        else if(InputManager_IsButtonDown(J_DOWN))
        {
            translation.y++;

            if (Movable_CanTryMoving(playerMovable))
                Movable_SetSpriteAnim(playerMovable, &SpriteAnim_PlayerMoveDown_Left, &SpriteAnim_PlayerMoveDown_Right);
        }
        else
        {
            Movable_SetSpriteAnim(playerMovable, NULL, NULL);
        }
            
        Movable_Translate(playerMovable, &translation, TRUE);
    }
}

/*
* Enable movement
*/
void EnableMovement()
{
    playerCanMove = TRUE;
}

/*
* Called when a dialog is closed
*/
void OnDialogClosed(Dialog* _Dialog)
{
    EnableMovement();
}