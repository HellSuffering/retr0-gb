if not exist build mkdir build
if not exist ROM mkdir ROM

C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/main.o Sources/main.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/MapLoader.o Sources/Engine/Maps/MapLoader.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Maps.o Sources/Engine/Maps/Assets/Maps.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/MapsCallbacks.o Sources/Engine/Maps/MapsCallbacks.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/InputManager.o Sources/Engine/Inputs/InputManager.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Movable.o Sources/Engine/Movables/Movable.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Collision.o Sources/Engine/Collision/Collision.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/CollisionCallbacks.o Sources/Engine/Collision/CollisionCallbacks.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Vector.o Sources/Engine/Math/Vector.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Box.o Sources/Engine/Math/Box.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Sprite.o Sources/Engine/Sprites/Sprite.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Game.o Sources/Engine/Game/Game.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/ScrollManager.o Sources/Engine/Scroll/ScrollManager.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/SpaceHelper.o Sources/Engine/Helpers/SpaceHelper.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/SpriteAnim.o Sources/Engine/Sprites/SpriteAnim.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Sprites_Animations.o Sources/Engine/Sprites/Assets/Sprites_Animations.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/BackgroundAnim.o Sources/Engine/Maps/BackgroundAnim.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Maps_Animations.o Sources/Engine/Maps/Assets/Maps_Animations.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Maps_Tilesets.o Sources/Engine/Maps/Assets/Maps_Tilesets.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Sprites_Tilesets.o Sources/Engine/Sprites/Assets/Sprites_Tilesets.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/DialogManager.o Sources/Engine/Dialogs/DialogManager.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Dialogs.o Sources/Engine/Dialogs/Assets/Dialogs.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/MusicPlayer.o Sources/Engine/Audio/MusicPlayer.c
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/Songs.o Sources/Engine/Audio/Assets/Songs.c

C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/gbt_player.o Sources/ThirdParty/gbtplayer/gbt_player.s
C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o build/gbt_player_bank1.o Sources/ThirdParty/gbtplayer/gbt_player_bank1.s

C:\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -o ROM/GameTemplate.gb build/main.o build/MapLoader.o build/InputManager.o build/Movable.o build/Box.o build/Vector.o build/Collision.o build/Sprite.o build/Maps.o build/CollisionCallbacks.o build/Game.o build/MapsCallbacks.o build/ScrollManager.o build/SpaceHelper.o build/SpriteAnim.o build/Sprites_Animations.o build/BackgroundAnim.o build/Maps_Animations.o build/Maps_Tilesets.o build/Sprites_Tilesets.o build/DialogManager.o build/Dialogs.o build/gbt_player.o build/gbt_player_bank1.o build/MusicPlayer.o build/Songs.o
pause