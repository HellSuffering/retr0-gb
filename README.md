# **Retr0 GB** #

Retr0 GB is a game engine using C and GBDK to make Gameboy game development a lot easier with a ready to use architecture.

## Features ##

* Code your game in C
* Manage sprites and/or box collision separately but in the same structure thanks to Movables (all non-static element as sprites, collision or both)
* Let your movables move freely pixel by pixel or constrain them on tiles (like RPG games) and let them interpolating their position themselves to next tile
* A complete collision system
	* Movable vs Movable blocking collision
	* Movable vs Movable overlap with automatic callback calls to manage easily what to do when movable A overlap movable B
	* Movable vs Map
	* Collision layers and layer masks to define which movable can collide with which other movable
* Sprite animations
* Animated tiles (animation for map background)
* Map loader to load map easily
* Calls to custom callbacks on map loaded and on update (called every frame) for each map
* Easy to use map scrolling, define which movable can scroll with the map and which movable can ask scrolling from its own movement
* Basic dialog system
* Music player ([gbt-player](https://github.com/AntonioND/gbt-player) implementation)
* Custom types as math 2D vectors or box
* Helper modules as functions to convert a screen position to tile position

## How do I get set up ? ##

Please refer to the [wiki setup page](https://bitbucket.org/HellSuffering/retr0-gb/wiki/Setup) to set up Retr0 GB. You can also visit the [wiki](https://bitbucket.org/HellSuffering/retr0-gb/wiki/Home) for more informations.

## Community ##

[Talk about Retr0 GB on discord](https://discord.gg/rVz9Hgx)

## License ##

Retr0 GB is distributed under zlib license

    Retr0 GB - Copyright (C)  K�vin Drure (alias Hell_666)
    
    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.

